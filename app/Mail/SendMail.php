<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(empty($data['code'])){
            return $this->from('cs@vretech.com')->subject('Verification Mail')->view('mail.mail');
        }
        else{
            return $this->from('cs@vretech.com')->subject('Contacting Mail')->view('mail.contacting');
        }

    }
}//,compact('data',$data)
