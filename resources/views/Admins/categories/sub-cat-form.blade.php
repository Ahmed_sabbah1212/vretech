<div class="form-group">
    <label for="subcategory_name">Category Name : {{$category->category_name}}</label>
</div>
    <div class="form-group pull-left col-lg-5 " style="display:inline-block;">
        <label for="subcategory_name">Sub Category Name</label>
        <input type="text" class="form-control" id="subcategory_name" value="" name ="subcategory_name" aria-describedby="subcategory_name" placeholder="Enter Sub category name in english">
        <div>{{ $errors->first('Subcategory_name') }}</div>
    </div>
    <div class="form-group  col-lg-5" class="pull-right" style="display:inline-block;">
        <label for="subcategory_name" class="pull-right">اسم التصنيف الفرعي</label>
        <input type="text" class="form-control " id="subcategory_name_ar" value="" name ="subcategory_name_ar" aria-describedby="subcategory_name" placeholder="برجاء ادخال اسم التصنيف الفرعي بالعربية">
        <div>{{ $errors->first('Subcategory_name') }}</div>
    </div>
<div class="form-group">
    <label for="image">Select Image</label>
    <input type="file" name="image" value = "" required id="image">
</div>
@csrf

