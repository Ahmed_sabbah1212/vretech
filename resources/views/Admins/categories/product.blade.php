@extends('layouts.layout')
@section('content')
<div class="row-fluid sortable">
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2 class="text-center"><i class="halflings-icon user"></i><span class="break"></span>{{ $SubCatName->subcategory_name }} Products</h2>
        </div>
        <div class="box-content">

            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                  <tr>
                    <th>#</th>
                    <th>Product Name</th>
                    <th>Available QYY</th>
                    <th>Available On</th>
                    <th>Status</th>
                    <th>category Name</th>
                    <th>SubCategory Name</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>View Details</th>
                    <th>Approval</th>
                    <th>Delete</th>
                  </tr>
              </thead>
              <?php $i=1;?>
                @foreach($products as $product)
                  <tbody>
                    <tr>
                        <td ><?php echo $i; $i=$i+1;?></td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->available_quantity }}</td>
                        <td>{{ $product->available_date }}</td>
                        <td >{{$product->status}}</td>
                        <td>{{$product->categories->category_name}}</td>
                        <td >{{$product->subcategory->subcategory_name}}</td>

                        <td >
                            @if(!empty($product->image))
                            <img src="{{url('images/products/'.$product->image)}}"class="make_bigger" width="100px" height="100px">
                        @else
                            <img src="{{ url('images/products/defualt.png') }}"width="100px" height="100px">
                        @endif
                        </td>
                        <td >{{$product->price}}</td>
                        <td>
                            <a href ="{{route('admin-view-product_details',$product->product_id)}}"><i class="fa fa-eye fa-4x" aria-hidden="true"></i></a>
                        </td>
                        <td>
                            @if($product->approval == 0 || $product->approval == 2)
                                <form action="{{ route('admin-accept-products',$product->product_id) }}     " method="POST">
                                    @method("POST")
                                    @csrf
                                    <button type="submit"  class="btn btn-outline-success btn-sm">Approve</button>
                                </form>
                            @else
                            <form action="{{ route('admin-hold-products',$product->product_id) }}" method="POST">
                                @method("POST")
                                @csrf
                                <button type="submit" class="btn btn-outline-warning btn-sm">Refuse</button>
                            </form>
                            @endif

                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>
                        <td>
                            <form action="{{route('admin-destroy-product',$product->product_id)}}" method="POST">
                                @method("DELETE")
                                @csrf
                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>

                            </form>
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>
                    </tr>
                  </tbody>
                @endforeach
          </table>
          {{--  {{ $products->links() }}  --}}

        </div>
    </div>
</div>

@endsection
