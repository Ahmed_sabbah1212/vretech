@extends('.site.layout.container')
@section('content')
@if (\Session::has('success'))
    <div class=" alert alert-success"style="text-align:center;">

            {!! \Session::get('success') !!}

    </div>
@endif
<div class="main-content bg-color">
    <div class="container">
    <div class="user-account text-center" style="">
    <div class="account-content">
    <div class="logo">
    <a href="/"><img class="img-fluid" src="{{asset("/assets/site")}}/images/logo_head.png" width="200px" height="100px" alt="Logo"></a>
    </div>
    <h1>{{trans('language.signin')}}</h1>
    <p>{{trans('language.subscribenow')}} </p>
    <form action="{{ url('login') }}" method="POST" class="tr-form"enctype="multipart/form-data" >
        @csrf
        <div class="form-group">
            <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="{{trans('language.email')}}" value="{{ old('email') }}" required = "required">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
        </div>
        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="{{trans('language.password')}}" required="required" autocomplete="current-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group ">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{trans('language.rememberme')}}
                    </label>

            </div>
        </div>
        <input type="submit" class="btn btn-primary" value="{{trans('language.signin')}} ">



        <div class="forgot-password">
            @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}" class="btn btn-link" >{{trans('language.forgetpassword')}}</a>

            @endif
        </div>



    </form>
    <span> {{trans('language.Donthaveanacount')}}<a href="{{ route('register') }}"><b>{{trans('language.Createone')}}</b></a></span>
    </div>
    </div>
    </div>
    </div>
@endsection
