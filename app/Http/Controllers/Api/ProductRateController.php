<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\ProductRate;
use Illuminate\Http\Request;
use Validator;
class ProductRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $rate = ProductRate::create($this->DataValidation());
        // trueresult(200,'Success request',$rate);
        $rules = [
            'product_id' =>'required|numeric|exists:products,product_id',
            'user_id' =>'required|numeric|exists:users,id',
            'api_token'=>'required|exists:users,api_token',
            'rate'=>'required|numeric|between:1,5'
        ];
        $validator = Validator::make( request()->all() , $rules);
        if($validator->fails()){
            falseResult(401,$validator->messages(),null);
        }
        else{
            $ProductRate = new ProductRate;
            $ProductRate->product_id =request('product_id') ;
            $ProductRate->user_id = request('user_id');
            $ProductRate->rate = request('rate');
            $ProductRate->save();
            trueResult(200,'success request',$ProductRate);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\ProductRate  $productRate
     * @return \Illuminate\Http\Response
     */
    public function show(ProductRate $productRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductRate  $productRate
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductRate $productRate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductRate  $productRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductRate $productRate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductRate  $productRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductRate $productRate)
    {
        //
    }
}
