@extends('layouts.layout')
@section('content')
<div class="container">
    <form action="{{ route('store-admin') }}"method="POST" enctype="multipart/form-data" class="col-lg-6" class="pb-5">


        <div class="form-group">
            <label for="name"> Name</label>
            <input type="text" class="form-control" id="name" value="" name ="name" aria-describedby="name" placeholder="Enter Admin Name">
            <div>{{ $errors->first('name') }}</div>
        </div>
        <div class="form-group">
            <label for="emial"> Emial</label>
            <input type="text" class="form-control" id="email" value="" name ="email" aria-describedby="email" placeholder="Enter Admin Email">
            <div>{{ $errors->first('email') }}</div>
        </div>
        <div class="form-group">
            <label for="password"> Password</label>
            <input type="password" class="form-control" id="password" value="" name ="password" aria-describedby="password" placeholder="Enter Admin password">
            <div>{{ $errors->first('password') }}</div>
        </div>

        @csrf


        <div class="input-group ">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection
