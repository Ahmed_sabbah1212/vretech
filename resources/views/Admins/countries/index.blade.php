@extends('layouts.layout')
@section('content')
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2 class="text-center"><i class="halflings-icon user"></i><span class="break"></span>All Countries</h2>
        </div>
        <div class="box-content">
            <form class="" method="get" action="{{url("/admin/countries")}}" style="max-width:400px">
                <div style="display: flex">
                        <input type="text" class="form-control" value="{{request()->country}}" name="country" placeholder="{{trans('Search..')}}">
                        <button type="submit" class="searchbtn"><i class="fa fa-search"></i></button>
                </div>
                <style>
                    form input[type=text] {
                        padding: 10px;
                        font-size: 17px;
                        border: 1px solid grey;
                        float: left;
                        width: 80%;
                        background: #f1f1f1;
                      }
                    form .searchbtn {
                        float: left;
                        width: 20%;
                        padding: 10px;
                        background: #1e8090;
                        color: white;
                        font-size: 17px;
                        border: 1px solid grey;
                        border-left: none;
                        cursor: pointer;
                      }
                      form button:hover {
                        background: #155d69;
                      }
                </style>
            </form>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                  <tr>
                      <th>#</th>
                    <th> Country Code</th>
                    <th> Name</th>
                  </tr>
              </thead>
              <?php $i=1;?>
                @foreach($countries as $country)
                  <tbody>
                    <tr>
                        <td ><?php echo $i; $i=$i+1;?></td>
                        <td>{{ $country->country_code }}</td>
                        <td ><a href="{{route('admin-show-country',$country->id)}}">{{$country->country_name}}</a></td>

                        <td>
                            <form action="{{route('admin-destroy-country',$country->id)}}" method="POST">
                                @method("DELETE")
                                @csrf
                                <button type="submit" onclick="return confirm('Please note : you will delete this country and it&rsquo;s other related functions just like cities, language and currency, Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>
                    </tr>
                  </tbody>
                @endforeach
          </table>
          {{ $countries->links() }}
        </div>
    </div>
</div>
@endsection
