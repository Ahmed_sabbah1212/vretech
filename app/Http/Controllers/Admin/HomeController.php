<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Products;
use App\User;
use App\Banners;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::take(5)->get();
        $products = Products::take(5)->get();
        $countries = Country::take(5)->get();
        $users        = User::take(5)->get();
        $banners = Banners::take(5)->get();

        return view('Admins.dashboard-home',compact('categories','products','countries','users','banners'));
    }
}
