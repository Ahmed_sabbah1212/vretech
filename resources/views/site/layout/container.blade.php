<!DOCTYPE html>
@if(app()->getLocale() == 'ar')
<html lang="{{ app()->getLocale() }}"  dir="rtl" class="rtl" >
@elseif(app()->getLocale() == 'en')
    <html lang="{{ app()->getLocale() }}"   >
@endif
@include('site.layout.head')
<body>
@include('site.layout.nav')

    @yield('content')
@include('site.layout.footer')
@include('site.layout.scripts')

</body>
</html>
