<div class="form-group">
    <label for="title">Banner Title </label>
    <input type="text" class="form-control" id="title" value="{{ old('title')?? $banner->title}}" name ="title" aria-describedby="title" placeholder="Enter Banner Title">
    <div>{{ $errors->first('title') }}</div>
</div>
<div class="form-group">
    <label for="link">Banner link </label>
    <input type="text" class="form-control" id="link" value="{{ old('link')?? $banner->link}}" name ="link" aria-describedby="link" placeholder="Enter Banner link">
    <div>{{ $errors->first('link') }}</div>
</div>
<div class="form-group">
    <label for="image">Select Image</label>
    <input type="file" name="image" value = "" id="image">
</div>
@csrf
