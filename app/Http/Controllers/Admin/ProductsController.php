<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Products;
use App\SubCategory;
use App\Category;
use Illuminate\Http\Request;
use Validator;
class ProductsController extends Controller
{

    public function __construct(){
        $this->middleware('auth:admin');
        // $this->middleware('guest:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Category $category )
    {
        $flag = array();
        $products = '';
        $data = $this->filter($request);
        if($request->product == null){
            $products = $data['products'];
        }else{
            $products = $this->search($request , $products);
        }
        $SubCategory = $data['SubCategory'];
        $flag = $data['flag'];
        $SubCatFlag = $data['SubCatFlag'];
        $categories = Category::all();

        return view('Admins.Products.index',compact('products','categories','SubCategory','flag','SubCatFlag'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {
        $id = (int)$product;
        $product = Products::where('product_id',$id)->first();
        // dd($product);
        return view('Admins.Products.product_details',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductDetails $productDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , Products $products)
    {
        $product = Products::where('product_id',request('product'));
        $product->delete();
        return back();
    }

    public function filter(Request $request)
    {
        $data = array();

        if($request->category){
            $products = Products::where('cat_id', (int)request('category'))->paginate(15);
            $SubCategory = SubCategory::where('category_id',(int)request('category'))->get();
            $flag =request('category');
            if ($request->subCategory){
                $products = Products::where('sub_cat_id', (int)request('subCategory'))->paginate(15);
                $SubCategory = SubCategory::where('id',(int)request('subCategory'))->get();
                $data = [
                    'products'=>$products,
                    'SubCategory'=>$SubCategory,
                    'flag'=>request('category'),
                    'SubCatFlag'=>request('subCategory'),
                ];
            }
            else{
                $data = [
                    'products'=>$products,
                    'SubCategory'=>$SubCategory,
                    'flag'=>$flag,
                    'SubCatFlag'=>'',
                ];
            }
        }
        else{
            $products = Products::paginate(15);
            $data = [
                'products'=>$products,
                'SubCategory'=>'',
                'flag'=>'',
                'SubCatFlag'=>'',
            ];
        }
        return $data;
    }
    public function search($request, $products)
    {


        if ($request->product) {
            $products = Products::where("name", 'LIKE', '%' . $request->product . '%')
                            ->orwhere("status", 'LIKE', '%' . $request->product . '%')
                            ->orwhere("available_quantity", 'LIKE', '%' . $request->product . '%')
                            ->orwhere("available_date", 'LIKE', '%' . $request->product . '%')
                            ->orwhere("location", 'LIKE', '%' . $request->product . '%')
                            ->paginate(10);
        }
        else{
            $products = Products::orderBy("product_id", "ASC")->paginate(10);
        }

        return $products;
    }

    public function accept(Request $request , Products $products){
        // dd(request('product'));
        $product = Products::where('product_id',request('product'))->update(['approval' => 1,'updated_at'=>date('Y-m-d H:i:s')]);
        return redirect('admin/products');
    }
    public function hold(Request $request , Products $products){
        //  dd(request('id'));
        $product = Products::where('product_id',request('id'))->update(['approval' => 2,'updated_at'=>date('Y-m-d H:i:s')]);
        return redirect('admin/products');
    }
}
