@extends('site.layout.container')
@section('content')
@include('site.layout.banner')
<div class="main-content popup-one">
<div class="container">
<div class="row">
<div class="row">
<div class="col-md-8 col-lg-9 push-md-4 push-lg-3">
<div class="tr-products">
<div class="row">
    @if (count($sub_categories) > 0 && count($products) > 0)
        <div class="pricing-content pricing-one section-bg-white col-md-12">
            <ul class="nav nav-tabs pricing-tabs" role="tablist">
                    <li class="nav-item">

                        <a class="nav-link active" href="{{ route('products',['category_name' => $sub_categories[0]->categories->category_name , 'category_id' => $sub_categories[0]->categories->id ]) }}"  aria-expanded="true">{{trans('language.all')}}</a>
                    </li>
                    @foreach ($sub_categories as $sub_category )
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('products',['category_name' => $sub_category->DashCate , 'category_id' => $sub_category->categories->id , 'sub_cat_id'=>$sub_category->id]) }}"  aria-controls="drinks">
                                <img class="img" src={{ url('images/categories/'.$sub_category->subcategory_image) }} style = "width:50px;height:50px;border-radius: 10%; margin-bottom:10px;">
                               <h6> {{ $sub_category->DashCate }}</h6>
                            </a>
                        </li>
                    @endforeach
            </ul>
        </div>



            @foreach ($products as$product )
            <div class="col-md-6 col-lg-4 {{ $product->sub_cat_id }}">
                <div class="product all ">
                        <span class="product-image">
                        <img src="{{ url('images/products/'.$product->image) }}" alt="Image" class="img-fluid">
                        </span>
                        <a href="{{route('show-product', $product->product_id)}}">
                            <span class="product-title">{{ $product->name }}</span>
                        </a>
                        <span class="price">{{ $product->price }}</span>
                        <div class="product-icon">
                        <ul class="global-list">
                        {{--  <li><a href="#"><span class="icon icon-love"></span></a></li>  --}}
                        {{--  <li><a href="{{ route('addToCart',$product->product_id) }}"><span class="icon icon-shopping-cart"></span></a></li>  --}}
                        <li><a class="image-link" href="{{ url('images/products/'.$product->image) }}"><span class="icon icon-full-screen"></span></a></li>
                        </ul>
                        </div>
                    </div>
                </div>
            @endforeach


        @else
            <div class="pricing-content text-center pricing-one  col-md-12">
                <p style="font-size: 1.6rem;">Sorry we have no items in this category</p>
                <button onclick="goBack()">Go Back</button>
                <script>
                function goBack() {
                window.history.back();
                }
                </script>
            </div>




    @endif
</div>
</div>
<div class="tr-pagination text-center">
    <ul class="pagination">
        {{ $products->links() }}
        {{-- @if ($products->onFirstPage())
            <li class="float-left"><a  class="disabled"><span>Prev</span></li>
        @else
        <li class="float-left"><a class="page-numbers" href="{{ $products->previousPageUrl() }}">Prev</a></li>
        @endif --}}

        {{-- @for ($i = 0 ; $i < $products->total(); $i++)
        <li class="active">
            <a class="page-numbers" href="{{ $products->url('vretech.local/categories/colors/55?page='.$i ) }}">{{ $i +1 }}</a>
        </li>
        @endfor --}}

        {{-- @if ($products->hasMorePages())
            <li class="float-right"><a class="page-numbers" href="{{ $products->nextPageUrl() }}">Next</a></li>
        @else
            <li class="float-right disabled"  ><span>Next</span></li>
        @endif --}}
    </ul>
    </div>
    </div>
        <div class="col-md-4 col-lg-3 pull-md-8 pull-lg-9">
            <div class="gb-sidebar">
                <div class="widget-area">
                    @include('site.includes.Categories')
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
