@extends('layouts.layout')
@section('content')
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2 class="text-center"><i class="halflings-icon user"></i><span class="break"></span>{{ $user->name }} Account</h2>
        </div>
        <div class="container">
            <form action="{{ route('admin-store-users',$user->id) }}"method="POST" enctype="multipart/form-data" class="col-lg-6" class="pb-5">
                <div class="form-group">
                    <label for="email">User Email</label>
                    <input type="text" class="form-control" id="email" value="{{ old('email')?? $user->email}}" name ="email" aria-describedby="email">
                    <div>{{ $errors->first('email') }}</div>
                </div>


                <div class="form-group">
                    <label for="messege">Messege</label>
                    <textarea class="form-control" name="messege" id="messege" cols="72" rows="10"></textarea>
                    <div>{{ $errors->first('messege') }}</div>
                </div>



                @csrf
                <div class="input-group ">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>

    </div>
</div>


@endsection
