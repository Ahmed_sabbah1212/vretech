<?php
namespace App\Http\Controllers\Site\Home;
use App\Http\Controllers\Controller;
use App\Products;
use App\SubCategory;
use App\Category;
use App\City;
use App\Cart;
use App\User;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use Session;

class CartController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
        // $this->middleware('guest:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $total_price = 0 ;
        $user_id = Auth::User()->id;
        $cart = Cart::where('user_id',$user_id)->get();
        $cart_items = DB::table('carts')
                    ->join('products','carts.product_id' , '=' , 'products.product_id')
                    ->select('products.product_id','carts.quantity','carts.days_num','carts.start_date','carts.end_date','products.name','products.image','products.price','products.status')->where('carts.order_id',null)->get();
        foreach($cart_items as $cart_item){
            $Splitted1 =  preg_split('#(?<=\d)()(?=[a-z])#i', $cart_item->price);
            $Splitted2 =  explode(" ",$Splitted1[0]);
            $cart_item->total_pricePerItem = $Splitted2[0] * $cart_item->days_num * $cart_item->quantity ;


            $product_img_array = explode('|', $cart_item->image);
            $cart_item->image =$product_img_array[0];
        }
        for($i = 0 ; $i < count($cart_items) ; $i++ ){
            $total_price = $total_price + $cart_items[$i]->total_pricePerItem;
        }

        return view('site.home.cart',compact('cart_items','total_price'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public  function show(Cart $cart)
    {
        $rules = [
            'useri_d'=>'required|numeric|exists:users,id',
            'api_token'=>'required'
        ];
        $items = array();
        $validator = Validator::make(request()->all(),$rules);
        if($validator->fails()){
            result(401,$validator->messages(),null);
        }
        if(CartController::isLogin(request('api_token'),request('user_id')) == true){
            $userCart = Cart::where('user_id',request('user_id'))->where('billing_status',null)->get();
            if(!empty($userCart)){
                foreach($userCart as $cart){
                    $item = Products::select('product_id','name','price','image')->where('product_id',$cart->product_id)->first();
                    $item_datailes = (object)[
                        'cart_id'=>$cart->id,
                        'quantity'=>$cart->quantity,
                        'product_id'=>$item->product_id,
                        'product_name'=>$item->name,
                        'product_price'=>$item->price,
                        'product_image'=>"http://www.vretech.com/images/products/".$item->image
                    ];
                    array_push($items,$item_datailes);
                }
                if(!empty($items)){
                    result(200,'Success request',$items);
                }else{
                    result(401,'User has No items in cart',null);
                }
            }
            else{
                result(401,'User has No items in cart',null);
            }
        }else{
            result(401,'Please Login first',null);
        }

    }


    public static function isLogin(){

        if(Auth::check())
        {
            return true;
        }
        else{
            return false;
        }

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy($cart_id)
    {
            if(CartController::isLogin() == true){
                    Cart::where('product_id',(int)$cart_id)->where('billing_status',null)->where('user_id',Auth::User()->id)->delete();
                    return back();
        }
    }
}
