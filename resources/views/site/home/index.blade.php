@extends('site.layout.container')
@section('content')

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{asset("/assets/site")}}/images/slider_bg.jpg" alt="First slide">
                <div class="carousel-caption  d-md-block">
                    <div class="banner-info">
                        <h1 data-animation="animated fadeInDown">{{trans('language.welcometovrtech')}}</h1>
                        <h2 data-animation="animated fadeInDown">{{trans('language.buildevent')}}</h2>
                        <p data-animation="animated fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset("/assets/site")}}/images/slider_bg.jpg" alt="Second slide">
                <div class="carousel-caption  d-md-block">
                    <div class="banner-info">
                        <h1 data-animation="animated fadeInDown">{{trans('language.welcometovrtech')}}</h1>
                        <h2 data-animation="animated fadeInDown">{{trans('language.buildevent')}}</h2>
                        <p data-animation="animated fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset("/assets/site")}}/images/slider_bg.jpg" alt="Third slide">
                <div class="carousel-caption  d-md-block">
                    <div class="banner-info">
                        <h1 data-animation="animated fadeInDown">{{trans('language.welcometovrtech')}}</h1>
                        <h2 data-animation="animated fadeInDown">{{trans('language.buildevent')}}</h2>
                        <p data-animation="animated fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>

    <div class="tr-farmfood farmfood-two section-padding bg-image" style="background-image: url(images/bg/farmfood.jpg);">
        <div class="container">
            <div class="section-title">
                <h1>{{trans('language.hereweare')}}</h1>
                <h2>{{trans('language.fullservice')}}</h2>
            </div>
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-md-6 col-lg-3">
                        <div class="farmfood">
                            <a href="{{ route('products',['category_name' => $category->category_name , 'category_id' => $category->id ]) }}">
                                <div class="icon">

                                    <img class="img" src={{ url('images/categories/'.$category->category_image) }} style = "width:120px;height:120px;">
                                </div>
                                <div class="food-info">
                                    <h3> <span>{{ $category->site_name }}</span></h3>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>


@endsection
