-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2020 at 11:44 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vretech`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ahmed sabbah', 'ahmed_sabbah121@outlook.com', NULL, '$2y$10$eE03/a5vv2OTIa3k5ShcH.EXgCu9517NvwjYeU7Te3Am6b583gQaS', 'YjvmFB1xPaPRpbZ9nSapJwBK6s2Prku10czeGshSIh3ABFEhD8ngJQdccbmX', NULL, NULL),
(3, 'ahmed sabbah', 'ahmedsabbah1212@gmail.com', NULL, '$2y$10$vdGJ2dv5qO5y30RSIjY0VeIXOcSAQVeBxwD1k.rlq6KrRjl/vB3U2', NULL, '2020-03-24 13:24:58', '2020-03-24 13:24:58'),
(4, 'yousefbkw', 'yousefbkw@gmail.com', NULL, '$2y$10$T6dZk8mUyjKFYjPbUD9Sy.eEfPxow8WYy9xYfgYkJLctVgoMjve2e', NULL, '2020-03-24 14:20:21', '2020-03-24 14:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT 'Banner Title',
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `link`, `image`, `created_at`, `updated_at`) VALUES
(19, 'Offers', 'Offers.com', 'CEZYxqTWkY67bUd1JPRW.png', '2020-03-21 17:49:46', '2020-03-21 17:49:47'),
(21, 'Offers', 'Offers.com', 'jA9XHY3teZBSgFfUPhDY.jpg', '2020-03-24 14:26:23', '2020-03-24 14:26:23');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `days_num` int(11) NOT NULL,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `billing_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `product_id`, `quantity`, `days_num`, `start_date`, `end_date`, `address`, `lat`, `lng`, `user_id`, `billing_status`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 3, '7/1/2020', '10/1/2020', 'cairo', '15.5', '13', 1, 'done', 1, '2020-01-23 19:26:05', '2020-01-23 19:46:28'),
(4, 1, 3, 3, '7/1/2020', '10/1/2020', 'cairo', '15.5', '13', 3, 'done', 1, '2020-01-24 05:48:58', '2020-01-24 05:48:58');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_image`, `created_at`, `updated_at`) VALUES
(49, 'Books', 'tYZhVKeEEvUBpwkq0yxy.jpg', '2020-03-24 09:20:22', '2020-03-24 09:20:22'),
(51, 'Mobiles', 'B2yIn21aydCeQYZTDZDS.jpeg', '2020-03-24 12:07:11', '2020-03-24 12:07:11'),
(52, 'Laptops', 'zUpCN6Y0YQ5augaPWOsy.jpeg', '2020-03-24 12:07:22', '2020-03-24 12:07:22'),
(53, 'Chairs', 'jkbtBLfyZFwD4itC4HGO.jpg', '2020-03-24 12:08:03', '2020-03-24 12:08:03'),
(55, 'colors', 'InGEogUxyz7x7b2lY1IH.jpg', '2020-03-24 14:28:05', '2020-03-24 14:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'cairo', 1, NULL, NULL),
(5, 'sadf', 1, '2020-02-23 17:49:05', '2020-02-23 17:49:05'),
(7, 'Dubai', 4, '2020-02-23 18:15:02', '2020-02-23 18:15:02'),
(8, 'Sharjah', 4, '2020-02-23 18:15:59', '2020-02-23 18:15:59'),
(9, 'Abu Dhabi', 4, '2020-02-23 18:16:27', '2020-02-23 18:16:27'),
(16, 'city 1', 2, '2020-03-17 20:38:41', '2020-03-17 20:38:41'),
(17, 'City 2', 2, '2020-03-17 20:38:50', '2020-03-17 20:38:50'),
(18, 'Dubai', 7, '2020-03-18 08:37:14', '2020-03-18 08:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`, `created_at`, `updated_at`) VALUES
(1, '1', 'Egypt', NULL, '2020-02-21 10:42:52'),
(2, '4', 'china', '2020-02-21 10:40:54', '2020-02-21 10:43:04'),
(3, '12', 'chinaaaa', '2020-02-23 17:49:31', '2020-02-23 17:49:54'),
(4, '971', 'united arab emirates', '2020-02-23 18:14:12', '2020-02-23 18:14:12'),
(6, '003', 'Country 4', '2020-03-17 20:39:06', '2020-03-17 20:39:06'),
(7, '971', 'UAE', '2020-03-18 08:36:57', '2020-03-18 08:36:57');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_01_182055_create_categories_table', 1),
(5, '2020_01_03_215629_create_banners_table', 1),
(6, '2020_01_04_000334_create_products_table', 1),
(7, '2020_01_11_192003_create_sub_categories_table', 1),
(8, '2020_01_14_184400_create_admins_table', 1),
(9, '2020_01_18_202354_create_cities_table', 1),
(10, '2020_01_18_204657_create_countries_table', 1),
(11, '2020_01_21_220722_create_carts_table', 1),
(12, '2020_01_22_212358_create_orders_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `total_price`, `created_at`, `updated_at`) VALUES
(1, 1, '18000', '2020-01-23 19:46:28', '2020-01-23 19:46:28'),
(9, 3, '18000', '2020-01-23 20:01:04', '2020-01-23 20:01:04');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_quantity` int(11) NOT NULL,
  `available_date` date DEFAULT current_timestamp(),
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `cat_id` int(11) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `location` varchar(191) CHARACTER SET utf32 DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approval` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `name`, `price`, `image`, `available_quantity`, `available_date`, `status`, `description`, `rate`, `cat_id`, `sub_cat_id`, `location`, `owner_id`, `created_at`, `updated_at`, `approval`) VALUES
(10, 'ULYSSES', '100 $', 'ULYSSES.jpg', 6, '2020-03-24', 'availabel', 'Written as an homage to Homer’s epic poem The Odyssey, Ulysses follows its hero, Leopold Bloom, through the streets of Dublin. Overflowing with puns, references to classical literature, and stream-of-consciousness writing, this is a complex, multilayered novel about one day in the life of an ordinary man. Initially banned in the United States but overturned by a legal challenge by Random House’s Bennett Cerf, Ulysses was called “a memorable catastrophe” (Virginia Woolf), “a book to which we are all indebted” (T. S. Eliot), and “the most faithful X-ray ever taken of the ordinary human consciousness” (Edmund Wilson). Joyce himself said, “There is not one single serious line in [Ulysses].', 5, 49, 16, 'cairo', 37, '2020-03-23 22:00:00', '2020-03-24 14:14:07', 1),
(11, 'THE GREAT GATSBY', '100 $', 'TheGreatGatsby2013Poster.jpg', 6, '2020-03-26', 'availabel', 'Written as an homage to Homer’s epic poem The Odyssey, Ulysses follows its hero, Leopold Bloom, through the streets of Dublin. Overflowing with puns, references to classical literature, and stream-of-consciousness writing, this is a complex, multilayered novel about one day in the life of an ordinary man. Initially banned in the United States but overturned by a legal challenge by Random House’s Bennett Cerf, Ulysses was called “a memorable catastrophe” (Virginia Woolf), “a book to which we are all indebted” (T. S. Eliot), and “the most faithful X-ray ever taken of the ordinary human consciousness” (Edmund Wilson). Joyce himself said, “There is not one single serious line in [Ulysses].', 5, 49, 17, 'Dubai', 37, '2020-03-23 22:00:00', NULL, 1),
(14, 'THE GREAT GATSBY', '100 $', 'TheGreatGatsby2013Poster.jpg', 6, '2020-03-26', 'availabel', 'Written as an homage to Homer’s epic poem The Odyssey, Ulysses follows its hero, Leopold Bloom, through the streets of Dublin. Overflowing with puns, references to classical literature, and stream-of-consciousness writing, this is a complex, multilayered novel about one day in the life of an ordinary man. Initially banned in the United States but overturned by a legal challenge by Random House’s Bennett Cerf, Ulysses was called “a memorable catastrophe” (Virginia Woolf), “a book to which we are all indebted” (T. S. Eliot), and “the most faithful X-ray ever taken of the ordinary human consciousness” (Edmund Wilson). Joyce himself said, “There is not one single serious line in [Ulysses].', 5, 49, 17, 'Dubai', 37, '2020-03-23 22:00:00', '2020-03-24 14:16:30', 2),
(15, 'THE GREAT GATSBY', '100 $', 'TheGreatGatsby2013Poster.jpg', 6, '2020-03-26', 'availabel', 'Written as an homage to Homer’s epic poem The Odyssey, Ulysses follows its hero, Leopold Bloom, through the streets of Dublin. Overflowing with puns, references to classical literature, and stream-of-consciousness writing, this is a complex, multilayered novel about one day in the life of an ordinary man. Initially banned in the United States but overturned by a legal challenge by Random House’s Bennett Cerf, Ulysses was called “a memorable catastrophe” (Virginia Woolf), “a book to which we are all indebted” (T. S. Eliot), and “the most faithful X-ray ever taken of the ordinary human consciousness” (Edmund Wilson). Joyce himself said, “There is not one single serious line in [Ulysses].', 5, 49, 17, 'Dubai', 37, '2020-03-23 22:00:00', NULL, 1),
(16, 'THE GREAT GATSBY', '100 $', 'TheGreatGatsby2013Poster.jpg', 6, '2020-03-26', 'availabel', 'Written as an homage to Homer’s epic poem The Odyssey, Ulysses follows its hero, Leopold Bloom, through the streets of Dublin. Overflowing with puns, references to classical literature, and stream-of-consciousness writing, this is a complex, multilayered novel about one day in the life of an ordinary man. Initially banned in the United States but overturned by a legal challenge by Random House’s Bennett Cerf, Ulysses was called “a memorable catastrophe” (Virginia Woolf), “a book to which we are all indebted” (T. S. Eliot), and “the most faithful X-ray ever taken of the ordinary human consciousness” (Edmund Wilson). Joyce himself said, “There is not one single serious line in [Ulysses].', 5, 49, 17, 'Dubai', 37, '2020-03-23 22:00:00', NULL, 1),
(18, 'THE GREAT GATSBY', '100 $', 'TheGreatGatsby2013Poster.jpg', 6, '2020-03-26', 'availabel', 'Written as an homage to Homer’s epic poem The Odyssey, Ulysses follows its hero, Leopold Bloom, through the streets of Dublin. Overflowing with puns, references to classical literature, and stream-of-consciousness writing, this is a complex, multilayered novel about one day in the life of an ordinary man. Initially banned in the United States but overturned by a legal challenge by Random House’s Bennett Cerf, Ulysses was called “a memorable catastrophe” (Virginia Woolf), “a book to which we are all indebted” (T. S. Eliot), and “the most faithful X-ray ever taken of the ordinary human consciousness” (Edmund Wilson). Joyce himself said, “There is not one single serious line in [Ulysses].', 5, 55, 21, 'Dubai', 37, '2020-03-23 22:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcategory_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `subcategory_name`, `subcategory_image`, `created_at`, `updated_at`) VALUES
(16, 49, 'Novels', 'wavRGB8XKZTepqW0sxaY.jpg', '2020-03-24 09:22:08', '2020-03-24 09:22:08'),
(17, 49, 'Fiction', '1CkehLaoq0kWmgg6KxmD.jpg', '2020-03-24 09:22:32', '2020-03-24 09:22:32'),
(21, 55, 'blue', '3bhRGQaV3x13vFNTc7xG.png', '2020-03-24 14:28:36', '2020-03-24 14:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify_code` int(11) NOT NULL,
  `messege` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `mobile`, `country_id`, `city_id`, `image`, `api_token`, `status`, `verify_code`, `messege`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'user1', 'user1@vretech.com', NULL, '0', '01094741234', 1, 1, '', 'WCzOopb3H8ozi4N5pFdjnU023Ss2qSudey5gZurT12BE16DsxEJPXI80FkZa', NULL, 0, NULL, NULL, '2020-01-23 19:22:22', '2020-01-23 19:58:43'),
(8, 'user6', 'user8@vretech.com', NULL, '0', '01155265126', 1, 1, '', 'YlFUHwk7W4gXCh7p2wLxKFjDmUUNW3kgkRAE43jUe6dUgsTnkF9yq1YbFC8e', NULL, 3344, NULL, NULL, '2020-01-24 15:57:37', '2020-01-24 15:57:37'),
(9, 'user6', 'user9@vretech.com', NULL, '0', '1155265126', 1, 1, '', 'kUB7mTr3OzLAilsPnPKovvDVrwmLuXjp9z3qAklMT9BHLMPbyd9CAEhoMsob', NULL, 2468, NULL, NULL, '2020-01-24 15:58:12', '2020-01-24 15:58:12'),
(10, 'user6', 'user10@vretech.com', NULL, '0', '1155265126', 1, 1, '', '7EdrbHexl8IPwcJOkLIc4FhdATaQwXkzbyMi6yXguEwArh6r8EmRNUdpVdgM', NULL, 39437, NULL, NULL, '2020-01-24 15:58:55', '2020-01-24 15:58:55'),
(11, 'user6', 'user11@vretech.com', NULL, '0', '1155265126', 1, 1, '', 'auFX3tD5r1eKX4pLc61n4xqQN0BlOCkH0XQycTi4ZUnw70YabN3O3ly3aLBq', NULL, 44651, NULL, NULL, '2020-01-24 16:01:54', '2020-01-24 16:01:54'),
(12, 'user6', 'user12@vretech.com', NULL, '0', '01094741234', 1, 1, '', 'PWHTP97qId771VB6LKJzrequFXJ9hABM67q0FI1Tce9Rje4GZPwxpWn3fvLq', NULL, 77593, NULL, NULL, '2020-01-24 16:05:32', '2020-01-24 16:05:32'),
(13, 'user6', 'user13@vretech.com', NULL, '0', '01155265126', 1, 1, '', 'fJfcyCz89qxquDscLhtjfW8Op1PjUVbeNnR2R1AuV5vCm7mHwImLeTgq6cZN', NULL, 56028, NULL, NULL, '2020-01-24 16:06:19', '2020-01-24 16:06:19'),
(14, 'user6', 'user14@vretech.com', NULL, '0', '1155265126', 1, 1, '', 'xMjMVTKhG161kFX2P4vyTxD5PeG0MbIXorNyWEGq60fJto3DGayzKePztjQC', NULL, 20068, NULL, NULL, '2020-01-24 16:06:59', '2020-01-24 16:06:59'),
(15, 'user6', 'user15@vretech.com', NULL, '0', '1155265126', 1, 1, '', 'LbIAKG3gqcbgCj0NSJqcJfv5lshpNmIqZS8i4e3lIv0ZtctfzpX77egBigfh', NULL, 59958, NULL, NULL, '2020-01-24 16:14:31', '2020-01-24 16:14:31'),
(16, 'user6', 'user16@vretech.com', NULL, '0', '1155265126', 1, 1, '', 'wlGgOT2GxG5taJPmqaGyC4NRcuE1rbPmUQb2rL11tn4FSryJrvVFGoiwnapb', NULL, 43149, NULL, NULL, '2020-01-24 16:15:01', '2020-01-24 16:15:01'),
(17, 'user6', 'user17@vretech.com', NULL, '0', '1155265126', 1, 1, '', '5eLoE2oGoVtvTMHC4H5CYOoOaQgJ2WVOcEPwr05JZcaaEUAmNUba8pzBc90W', NULL, 7934, NULL, NULL, '2020-01-24 16:17:08', '2020-01-24 16:17:08'),
(29, 'user6', 'ahmed_Sabbah182@outlook.com', NULL, '0', '1155265126', 1, 1, '', 'AnCDzdNS5tdRGbZpnfx72h1BwfXWJI2bfFT8crX5F9zZuQUBr6uyTPsi1Y6B', NULL, 6548, NULL, NULL, '2020-01-24 17:33:49', '2020-01-24 17:33:49'),
(30, 'user6', 'ahmed_Sabba0@outlook.com', NULL, '0', '1155265126', 1, 1, '', 'PejhJytFbDmeOk03RKFB69llJHa1XXgoszK0lRBQqbkZu8KVsT173Omy3CL5', NULL, 4598, NULL, NULL, '2020-01-24 17:34:59', '2020-01-24 17:34:59'),
(36, 'ahmed sabbah', 'ahmedSabbah1212@gmail.com', NULL, '0', '1155265126', 1, 1, '1580165430.jpeg', 'pduIHVw0OSO1vBHKyFQwYmFuZuMHkmrL3DIE6bBk8xy9BghbTUSXuk2UxJdY', NULL, 2532, NULL, NULL, '2020-01-27 20:50:30', '2020-01-27 20:50:30'),
(37, 'ahmed sabbah', 'ahmed_Sabbah121@outlook.com', NULL, '0', '1155265126', 1, 1, '1580362939.jpeg', 'e2cpRU03vC2Sv9yZSXUbZLQII0wOY9i1sR4H7VTgv2P8TTcLVAnkx3UpPukh', NULL, 4579, NULL, NULL, '2020-01-30 03:42:20', '2020-01-30 22:53:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
