@extends('site.layout.container')
@section('content')
@include('site.layout.banner')
{{--
{{trans('language.hereweare')}}
--}}
@if (Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
   @elseif(Session::has('error'))
   <div class="alert alert-danger text-center" >{{ Session::get('error') }}</div>
@endif
<div class="main-content bg-color">
<div class="container">
<div class="row">
<div class="col-md-8 col-lg-8  push-md-4 push-lg-3">
<div class="details-content">
<div class="product-details section-bg-white">
<div class="row">
<div class="col-lg-6">
<div id="details-slider" class="details-slider carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
    <li data-target="#details-slider"  class="active"><img src="{{ url('images/products/'.$productDetails->image[0]) }}" alt="Image" class="img-fluid"></li>

    @for($i = 1 ; $i<count($productDetails->image) ;$i ++ )
        <li data-target="#details-slider" ><img src="{{ url('images/products/'.$productDetails->image[$i]) }}" alt="Image" class="img-fluid"></li>
    @endfor
</ol>
<div class="carousel-inner" role="listbox">
<div class="carousel-item item active">
    <img class="img-fluid" src="{{ url('images/products/'.$productDetails->image[0]) }}" alt="Image">
</div>
<div class="carousel-item">
    @for($i = 1 ; $i<count($productDetails->image) ;$i ++ )
        <img class="img-fluid" src="{{ url('images/products/'.$productDetails->image[$i]) }}" alt="Image">
    @endfor
</div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="product-details-info">
<span class="product-title" style = "font-family:Candara;"><strong>{{ $productDetails->name }}</strong></span>
@if( $productDetails->current_stock < 1 )

<span style="color:#969696;">Sorry Item is not available Now</span>

<span class="price">{{ $productDetails->price  }}</span>

    <p>Some important info about our product</p>
    <ul class="global-list">
        <li>Product Exists in {{ $productDetails->location }}</li>
    </ul>
</div>
</div>
</div>
</div>



<div class="products-description section-bg-white">
    <ul class="nav nav-tabs description-tabs" role="tablist">
    <li role="presentation" class="nav-item"><a class="nav-link active" href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#payment" aria-controls="payment" role="tab" data-toggle="tab" aria-expanded="true">Payment</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#delivery" aria-controls="delivery" role="tab" data-toggle="tab" aria-expanded="true">Delivery</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade show active" id="details">
            <p>{{ $productDetails->description  }}</p>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="payment">
            <div class="payment">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="delivery">
            <div class="delivery">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
            </div>
        </div>
    </div>
@else

<div class="review-rating">
    <div class="star">
        <div class="star-bg">
            <span>Product Rate</span>
            <div class="clearfix"></div>
            @for($i = 0 ; $i < $productRate['sum'] ; $i++)
                <i class="fa fa-star" style="color: #f1c40f"></i>
            @endfor
        </div>
        <span>Rate Us</span>
            <div class="clearfix"></div>

    </div>
</div>
<div class="staravg" style= "padding-left: 8px;">
    <a class=" fa fa-star b1" href="{{ route('rate',[5,$productDetails->product_id]) }}" value = "5" ></a>
    <a class=" fa fa-star b2" href="{{ route('rate',[4,$productDetails->product_id]) }}" value = "4" ></a>
    <a class=" fa fa-star b3" href="{{ route('rate',[3,$productDetails->product_id]) }}" value = "3" ></a>
    <a class=" fa fa-star b4" href="{{ route('rate',[2,$productDetails->product_id]) }}" value = "2" ></a>
    <a class=" fa fa-star b5" href="{{ route('rate',[1,$productDetails->product_id]) }}" value = "1" ></a>

</div>

<style>
    .staravg a{

        font-size: 15px;
        color: #313131 !important;
        translation: 0.1s all;

        transform: translate(-50% ,-50%);
        direction:ltr !important;
    }

    .staravg a:hover{
        color: #f1c40f !important;
    }

    .b1:hover ~ a{
        color: #f1c40f !important;
    }
    .b2:hover ~ a{
        color: #f1c40f !important;
    }
    .b3:hover ~ a{
        color: #f1c40f !important;
    }
    .b4:hover ~ a{
        color: #f1c40f !important;
    }
    .b5:hover ~ a{
        color: #f1c40f !important;
    }



</style>
    <span style="color: green;">Available</span>
    <span class="price">{{ $productDetails->price  }}</span>
    <strong>Current Stock</strong><span class="price">{{ $productDetails->current_stock }}</span>


    <p>Some important info about our product</p>
    <ul class="global-list">
        <li>Product Exists in {{ $productDetails->location }}</li>
    </ul>

    @guest
    <a class=" btn btn-success" style="margin-top:50px;" href="{{ route('login') }}">Please Sign in to get this item</a>
    @else
        @if ($productDetails->owner_id == $user_id)
            <div class="col-sm-12" style="inline-block; margin-top:50px;">
                <a href="{{ route('product.edit',$productDetails->product_id) }}" class="col-sm-4 btn btn-warning">Edit</a>


                <form action="{{route('destroy-product',$productDetails->product_id)}}" method="POST">
                    @method("DELETE")
                    @csrf
                    <button type="submit" onclick="return confirm('do you want to delete this product?')" class="col-sm-4 btn btn-danger">Delete</button>
                </form>
                @if($errors->any())
                <h4>{{$errors->first()}}</h4>
                @endif
            </div>


        @else
        @if ($cartStatus == 0)
        <form action="{{ route('addToCart',$productDetails->product_id) }}" method="POST">
            @csrf
            <br>
            <p>Please fill these inputs if you want to add to cart</p>
            <div class="quantity-price city-select form-check-inline">
                <span>City</span>
                <select id="inputState" name = "address"class="form-control" required>
                        <option selected disabled>Select City</option>
                        @foreach ($cities as $city )
                        <option required>{{ $city->city_name }}</option>
                        @endforeach

                    </select>
                </div>


                <div class="quantity-price city-select form-check-inline">
                        <span>Start date</span>
                    <input type="date" name = "start_date" class="form-control" required>
                </div>


                <div class="quantity-price city-select form-check-inline">
                        <span>End date</span>
                    <input type="date" name = "end_date"class="form-control" required>
                </div>


                <div class="quantity-price">
                    <span>Quality</span>
                    <div class="quantity" data-trigger="spinner">
                        <a href="#" data-spin="down"><i class="fa fa-minus"></i></a>
                        <input type="text" name="quantity" value="1" title="quantity" class="input-text" required>
                        <a href="#" data-spin="up"><i class="fa fa-plus"></i></a>
                    </div>
                </div>

                <div class="add-to-cart">
                    <button class="btn btn-primary" type="submit">Add to Cart</button>
                </div>
            </form>
            @else
            <br>
                <div class="remove-from-cart">
                    <a href="{{ route('remove_from_cart',$productDetails->product_id) }}" class="btn btn-danger">Remove from Cart</a>
                </div>
            @endif
    @endif
@endguest
</div>
</div>
</div>
</div>



<div class="products-description section-bg-white">
    <ul class="nav nav-tabs description-tabs" role="tablist">
    <li role="presentation" class="nav-item"><a class="nav-link active" href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#payment" aria-controls="payment" role="tab" data-toggle="tab" aria-expanded="true">Payment</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#delivery" aria-controls="delivery" role="tab" data-toggle="tab" aria-expanded="true">Delivery</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade show active" id="details">
            <p>{{ $productDetails->description  }}</p>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="payment">
            <div class="payment">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="delivery">
            <div class="delivery">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
            </div>
        </div>
    </div>


@endif
</div>
</div>
</div>
<div class="col-md-4 col-lg-4 pull-md-8 pull-lg-9">
<div class="gb-sidebar">
<div class="widget-area">
        <span class="product-title"> <strong>Select Location on Map</strong></span>

        <div style="width: 100%">
            <iframe width="100%" height="300" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street%2C%20Dublin%2C%20Ireland+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                <a href="https://www.maps.ie/draw-radius-circle-map/">Radius map tool</a>
            </iframe>
        </div>
            <br />
        @include('site.includes.Categories')
</div>
</div>
</div>
</div>
</div>
</div>
@endsection

