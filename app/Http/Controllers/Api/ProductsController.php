<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Products;
use App\SubCategory;
use App\ProductRate;
use Illuminate\Http\Request;
use Validator;
use Str;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules =[
            'sub_cat_id' =>'nullable|exists:sub_categories,id',
            'cat_id' =>'nullable|exists:categories,id'
        ];
        $products = array();
        $validator = Validator::make(request()->all() , $rules);
        if($validator->fails()){
            falseResult(401,$validator->messages(), null);
        }
        else{

            if(request('cat_id') != null && request('sub_cat_id') == null){
                $products =  $this->productFilterWithCatOnly(request('cat_id'));
                trueresult ('200','success',$products);
            }
            elseif(request('cat_id') == null && request('sub_cat_id') != null){
                $products = $this->productFilterWithSubCatOnly(request('sub_cat_id'));
                trueresult ('200','success',$products);
            }
            else{
                falseResult(401,"Please check your input", null);
            }
        }
    }

    public function getRate($product_id){
        $rate_count = ProductRate::where('product_id', $product_id)->count();
        if($rate_count > 0){
            $rate_sum = ProductRate::where('product_id', $product_id)->sum('rate');
            $rate = $rate_sum/$rate_count;
            return round($rate);
        }
        else{
            return null;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' =>'required|numeric|exists:users,id',
            'api_token'=>'required|exists:users,api_token',
            'product_id'=>'nullable|numeric|exists:products,id',
            'name'=>'required',
            'price'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'current_stock'=>'required|numeric',
            'available_at'=>'required|date',
            'new_available_qty'=>'required|numeric',
            'status'=>'required',
            'description'=>'required|min:3|max:255',
            'cat_id'=>'required|exists:categories,id',
            'sub_cat_id'=>'required|exists:sub_categories,id',
            'location'=>'required|min:3|max:191',
        ];
        $validator = Validator::make( request()->all() , $rules);
        if($validator->fails()){
            falseResult(401,$validator->messages(),null);
        }
        else{
            $image = request('image');
            $image_name = Str::random(20);
			$exe = strtolower($image->getClientOriginalExtension());
			$image_full_name = $image_name.'.'.$exe;
			$upload_path = 'images/products';
			$image_url = $image_full_name;
            $image->move($upload_path,$image_full_name);
            $product = new Products;
            $product->name =  request('name');
            $product->price =  request('price');
            $product->image =  $image_url;
            $product->current_stock =  request('current_stock');
            $product->available_date =  request('available_at');
            $product->new_available_qty =  request('new_available_qty');
            $product->status =  request('status');
            $product->description =  request('description');
            $product->cat_id =  request('cat_id');
            $product->sub_cat_id =  request('sub_cat_id');
            $product->location =  request('location');
            $product->owner_id =  request('user_id');
            $product->save();
            $product->image ='http://www.vretech.com/images/products/'.$product->image;


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        $rules =[
            'product_id' =>'required|numeric',
        ];
        $validator = Validator::make(request()->all() , $rules);
        if($validator->fails()){
            falseResult(401,'Validation Error please check your input');
        }
        else{
            $product_details =  Products::where('product_id',Request('product_id'))->first();
            if($product_details){

            $product = [
                'product_id'=> $product_details->product_id,
                'name'=> $product_details->name,
                'price'=> $product_details->price,
                'image'=>'http://www.vretech.com/images/products/'.$product_details->image,
                'current_stock'=> $product_details->current_stock,
                'available_date' =>$product_details->available_date,
                'new_available_qty'=> $product_details->new_available_qty,
                'rate' =>$this->getRate($product_details->product_id),
                'status'=>$product_details->status,
                'sub_cat_id'=>$product_details->sub_cat_id,
                'sub_cat_name'=>$product_details->subcategory->subcategory_name,
                'sub_cat_image'=>'http://www.vretech.com/images/sub-categories/'.$product_details->subcategory->subcategory_image,
                'cat_id'=>$product_details->cat_id,
                'cat_name'=>$product_details->categories->category_name,
                'cat_image'=>'http://www.vretech.com/images/categories/'.$product_details->categories->category_image,
                'created_at'=> $product_details->created_at,
                'updated_at'=>$product_details->updated_at,
            ];

                trueresult(200,'Success request',$product);
            }
            else{
                falseResult(401,'No Data hes been found');
            }
        }
    }



    public function view(Products $products){
        $rules =[
            'user_id' =>'required|numeric|exists:users,id',
            'api_token'=>'required|exists:users,api_token'
        ];
        $validator = Validator::make(request()->all() , $rules);
        if($validator->fails()){
            falseResult(401,$validator->messages());
        }else{
            $all_products = array();
            $products = Products::where('owner_id',request('user_id'))->where('deleted_at',null)->get();
            if(!empty($products)){
                for($j = 0 ; $j <count($products) ; $j++){
                    $single_product = (object)[
                                'product_id'=> $products[$j]->product_id,
                                'name'=> $products[$j]->name,
                                'price'=> $products[$j]->price,
                                'image'=>'http://www.vretech.com/images/products/'.$products[$j]->image,
                                'current_stock'=> $products[$j]->current_stock,
                                'available_date' =>$products[$j]->available_date,
                                'new_available_qty'=> $products[$j]->new_available_qty,
                                'rate' =>$this->getRate($products[$j]->product_id),
                                'status'=>$products[$j]->status,
                                'sub_cat_id'=>$products[$j]->sub_cat_id,
                                'sub_cat_name'=>$products[$j]->subcategory->subcategory_name,
                                'sub_cat_image'=>'http://www.vretech.com/images/sub-categories/'.$products[$j]->subcategory->subcategory_image,
                                'cat_id'=>$products[$j]->cat_id,
                                'cat_name'=>$products[$j]->categories->category_name,
                                'cat_image'=>'http://www.vretech.com/images/categories/'.$products[$j]->categories->category_image,
                                'created_at'=> $products[$j]->created_at,
                                'updated_at'=>$products[$j]->updated_at,
                            ];
                            array_push($all_products, $single_product);
                }
                trueresult(200,'Success request',$all_products);
            }
            else{
                trueresult(200,'Success request but no data found for this user',null);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductDetails $productDetails)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products)
    {
        $rules = [
            'user_id' =>'required|numeric|exists:users,id',
            'api_token'=>'required|exists:users,api_token',
            'product_id'=>'nullable|numeric|exists:products,product_id',
            'name'=>'required',
            'price'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'current_stock'=>'required|numeric',
            'available_at'=>'required|date',
            'new_available_qty'=>'required|numeric',
            'status'=>'required',
            'description'=>'required|min:3|max:255',
            'cat_id'=>'required|exists:categories,id',
            'sub_cat_id'=>'required|exists:sub_categories,id',
            'location'=>'required|min:3|max:191',
        ];
        $validator = Validator::make( request()->all() , $rules);
        if($validator->fails()){
            falseResult(401,$validator->messages(),null);
        }
        else{
            $image = request('image');
            $image_name = Str::random(20);
			$exe = strtolower($image->getClientOriginalExtension());
			$image_full_name = $image_name.'.'.$exe;
			$upload_path = 'images/products';
			$image_url = $image_full_name;
            $image->move($upload_path,$image_full_name);
            Products::where('product_id' ,request('product_id'))
            ->update(
                [
                    'name' => request('name'),
                    'price' => request('price'),
                    'image'=> $image_url,
                    'current_stock'=> request('current_stock'),
                    'available_date' =>  request('available_at'),
                    'new_available_qty' =>  request('new_available_qty'),
                    'status' =>  request('status'),
                    'description' =>  request('description'),
                    'cat_id' =>  request('cat_id'),
                    'sub_cat_id' =>  request('sub_cat_id'),
                    'location' =>  request('location'),
                    'owner_id' =>  request('user_id'),
                ]
            );
            $product = Products::where('product_id',request('product_id'))->first()->exclude(['approval']);
            $product->image = 'http://www.vretech.com/images/products/'.$product->image;
            $product->rate  =  $this->getRate($product->product_id);
            trueresult(200,'Product updated successfully',$product);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products)
    {
        $rules =[
            'product_id' =>'required|numeric',
            'user_id' =>'required|numeric|exists:users,id',
            'api_token'=>'required|exists:users,api_token'
        ];
        $validator = Validator::make(request()->all() , $rules);
        if($validator->fails()){
            falseResult(401,'Validation Error please check your input',null);
        }
        else{
            $product = Products::where('product_id', request('product_id'))->delete();
            trueresult(200,'Product deleted Successfully',null);
        }
    }
    public  function productFilterWithCatOnly($cat_ids){
        $cat_ids = explode(",",$cat_ids);
        $all_products = array();
        foreach($cat_ids as $cat_id)
        {
            $products = Products::where('cat_id',$cat_id)->where('approval',1)->get();
            array_push($all_products, $products);
        }
        if($all_products){
            return $this->ProductTaitment($all_products);
        }
        else{
            return 'no dat found';
        }
    }
    public function productFilterWithSubCatOnly($sub_cat_ids){
        $all_products = array();
        $sub_cat_ids = explode(",",$sub_cat_ids);
        foreach($sub_cat_ids as $sub_cat_id)
        {
            $product  = Products::where('sub_cat_id' , $sub_cat_id)->where('approval',1)->get();
            array_push($all_products, $product);
        }
        return $this->ProductTaitment($all_products);
    }
    public function ProductTaitment($all_products){
        $all_customized_products = array();
        foreach($all_products as $products){
            for($j = 0 ; $j < count($products); $j++){
                $single_product = (object)[
                    'product_id'=> $products[$j]->product_id,
                    'name'=> $products[$j]->name,
                    'price'=> $products[$j]->price,
                    'image'=>'http://www.vretech.com/images/products/'.$products[$j]->image,
                    'current_stock'=> $products[$j]->current_stock,
                    'available_date' =>$products[$j]->available_date,
                    'new_available_qty'=> $products[$j]->new_available_qty,
                    'rate' => $this->getRate($products[$j]->product_id),
                    'status'=>$products[$j]->status,
                    'sub_cat_id'=>$products[$j]->sub_cat_id,
                    'sub_cat_name'=>$products[$j]->subcategory->subcategory_name,
                    'sub_cat_image'=>'http://www.vretech.com/images/sub-categories/'.$products[$j]->subcategory->subcategory_image,
                    'cat_id'=>$products[$j]->cat_id,
                    'cat_name'=>$products[$j]->categories->category_name,
                    'cat_image'=>'http://www.vretech.com/images/categories/'.$products[$j]->categories->category_image,
                    'owner_id'=>$products[$j]->owner_id,
                    'created_at'=> $products[$j]->created_at,
                    'updated_at'=>$products[$j]->updated_at,
                ];
                array_push($all_customized_products,$single_product);
            }
        }
        return $all_customized_products;
    }







    public function ProductTraitement2($products){
        $all_products = array();
        $single_product = (object)[];
        for($i = 0 ; $i < count($products) ; $i++){}


        for($j = 0 ; $j < count($products) ; $j++){
            $single_product = (object)[
                        'product_id'=> $products[$j]->product_id,
                        'name'=> $products[$j]->name,
                        'price'=> $products[$j]->price,
                        'image'=>'http://www.vretech.com/images/products/'.$products[$j]->image,
                        'current_stock'=> $products[$j]->current_stock,
                        'available_date' =>$products[$j]->available_date,
                        'new_available_qty'=> $products[$j]->new_available_qty,
                        'rate' => $this->getRate($products[$j]->product_id),
                        'status'=>$products[$j]->status,
                        'sub_cat_id'=>$products[$j]->sub_cat_id,
                        'sub_cat_name'=>$products[$j]->subcategory->subcategory_name,
                        'sub_cat_image'=>'http://www.vretech.com/images/sub-categories/'.$products[$j]->subcategory->subcategory_image,
                        'cat_id'=>$products[$j]->cat_id,
                        'cat_name'=>$products[$j]->categories->category_name,
                        'cat_image'=>'http://www.vretech.com/images/categories/'.$products[$j]->categories->category_image,
                        'owner_id'=>$products[$j]->owner_id,
                        'created_at'=> $products[$j]->created_at,
                        'updated_at'=>$products[$j]->updated_at,
                    ];
                    array_push($all_products, $single_product);
        }
        return ($all_products);
    }


}
