jQuery(function ($) {
    "use strict";
    (function () {
        $(".search-icon").on("click", function () {
            if ($(this).hasClass("opened")) {
                $(this).removeClass("opened");
                $(".search-icon i").removeClass("active-search").addClass("fa-search");
                $(".search-form").fadeOut("slow").removeClass("").addClass("");
            } else {
                $(this).addClass("opened");
                $(".search-icon i").addClass("active-search").removeClass("fa-search");
                $(".search-form").fadeIn("slow").removeClass("").addClass("");
            }
        });
        $("body").on("click", function () {
            $(".search-icon").removeClass("opened");
            $(".search-icon i").removeClass("active-search").addClass("fa-search");
            $(".search-form").fadeOut("slow").removeClass("").addClass("");
        });
        $(".tr-search").on("click", function (e) {
            e.stopPropagation();
        });
    })();
    mobileDropdown();
    function mobileDropdown() {
        if ($(".tr-menu .navbar-nav").length) {
            $(".tr-menu .navbar-nav .tr-dropdown").append(function () {
                return '<i class="fa fa-angle-down icon" aria-hidden="true"></i>';
            });
            $(".tr-menu .navbar-nav .tr-dropdown .icon").on("click", function () {
                $(this).parent("li").children("ul").slideToggle();
            });
        }
    }
    (function () {
        $(".product-slider, .blog-slider").slick({ infinite: true, dots: true, slidesToShow: 1, autoplay: true, autoplaySpeed: 10000, slidesToScroll: 1 });
        $(".brand-slider").slick({
            infinite: true,
            dots: true,
            slidesToShow: 4,
            autoplay: true,
            autoplaySpeed: 10000,
            slidesToScroll: 1,
            responsive: [
                { breakpoint: 1025, settings: { slidesToShow: 3 } },
                { breakpoint: 768, settings: { slidesToShow: 2 } },
                { breakpoint: 475, settings: { slidesToShow: 1 } },
            ],
        });
    })();
    (function () {
        $(".countdown").countdown({ date: "29 Dec 2025 12:00:00", format: "on" });
    })();
    (function () {
        $(".remove-icon").on("click", function () {
            $(this).parents(".remove-item").fadeOut();
        });
    })();
    (function () {
        $(".popup-one .image-link").magnificPopup({ type: "image", gallery: { enabled: true } });
        $(".popup-two .image-link").magnificPopup({ type: "image", gallery: { enabled: true } });
        $(".video-link").magnificPopup({ type: "iframe" });
    })();
    $(function () {
        $("#price_slider").slider({
            range: true,
            min: 0,
            max: 1050,
            values: [0, 1050],
            slide: function (event, ui) {
                $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            },
        });
        $("#amount").val("$" + $("#price_slider").slider("values", 0) + " - $" + $("#price_slider").slider("values", 1));
    });
    $(function () {
        $("#home-carousel").bsTouchSlider();
        // $("#home-carousel").slick({ 
        //     nav:true });
    });
    (function ($) {
        function doAnimations(elems) {
            var animEndEv = "webkitAnimationEnd animationend";
            elems.each(function () {
                var $this = $(this),
                    $animationType = $this.data("animation");
                $this.addClass($animationType).one(animEndEv, function () {
                    $this.removeClass($animationType);
                });
            });
        }
        var $myCarousel = $("#home-carousel"),
            $firstAnimatingElems = $myCarousel.find(".item:first").find("[data-animation ^= 'animated']");
        $myCarousel.carousel();
        doAnimations($firstAnimatingElems);
        $myCarousel.carousel("pause");
        $myCarousel.on("slide.bs.carousel", function (e) {
            var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
            doAnimations($animatingElems);
        });
    })(jQuery);
});
