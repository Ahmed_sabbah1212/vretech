@extends('layouts.layout')
@section('content')
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2 class="text-center"><i class="halflings-icon user"></i><span class="break"></span>All Users</h2>
        </div>
        <div class="box-content">
            <form class="" method="get" action="{{url("/admin/users")}}" style="max-width:400px">
                <div style="display: flex">
                        <input type="text" class="form-control" value="{{request()->user}}" name="user" placeholder="{{trans('Search..')}}">
                        <button type="submit" class="searchbtn"><i class="fa fa-search"></i></button>
                </div>
                <style>
                    form input[type=text] {
                        padding: 10px;
                        font-size: 17px;
                        border: 1px solid grey;
                        float: left;
                        width: 80%;
                        background: #f1f1f1;
                      }
                    form .searchbtn {
                        float: left;
                        width: 20%;
                        padding: 10px;
                        background: #1e8090;
                        color: white;
                        font-size: 17px;
                        border: 1px solid grey;
                        border-left: none;
                        cursor: pointer;
                      }
                      form button:hover {
                        background: #155d69;
                      }
                </style>
            </form>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                  <tr>
                    <th>User ID</th>
                    <th> Name</th>
                    <th> Email</th>
                    <th> Phone</th>
                    <th> Address</th>
                    <th> status</th>
                    <th> Image</th>
                    <th> Contact</th>
                    <th> Delete</th>
                  </tr>
              </thead>
                @foreach($users as $user)
                  <tbody>
                    <tr>
                        <td >{{$user->id}}</td>
                        <td >{{$user->name}}</td>
                        <td >{{$user->email}}</td>
                        <td >{{$user->mobile}}</td>
                        <td >{{ $user->city->city_name}}, {{ $user->country->country_name}}</td>
                        <td >{{$user->status}}</td>
                        <td >
                            @if(empty($user->image))
                                <img src="{{url('images/users/user.png')}}" width="100px" height="100px">
                            @else
                                <img src="{{url('images/users/'.$user->image)}}" width="100px" height="100px">
                            @endif
                        </td>
                        <td>
                            <form action="{{ route('admin-contact-users',$user->id) }}" method="GET">
                                @method("GET")
                                @csrf
                                <button type="submit"class="btn btn-outline-success btn-sm">Contact</button>

                            </form>
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>

                        <td>
                            <form action="{{ route('admin-destroy-users',$user->id) }}" method="POST">
                                @method("DELETE")
                                @csrf
                                <button type="submit"onclick="return confirm('Are you sure?')"class="btn btn-outline-danger btn-sm">Delete</button>

                            </form>
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>
                    </tr>
                  </tbody>
                @endforeach
          </table>
            <div >
                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
