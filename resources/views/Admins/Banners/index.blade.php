@extends('layouts.layout')
@section('content')

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2 class="text-center"><i class="halflings-icon user"></i><span class="break"></span>All Banners</h2>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                  <tr>
                    <th> #</th>
                    <th> Banner Name</th>
                    <th> Banner Link</th>
                    <th> Banner Image</th>
                  </tr>
              </thead>
              <?php $i=1;?>
                @foreach($banners as $banner)
                  <tbody>
                    <tr>
                        <td ><?php echo $i; $i=$i+1;?></td>
                        <td >{{$banner->title}}</td>
                        <td >{{$banner->link}}</td>
                        <td >
                            @if(empty($banner->image))
                                <img src="{{url('images/banners/banner.png')}}" width="120px" height="100px">
                            @else
                                <img src="{{ url('images/banners/'.$banner->image) }}"width="120px" height="100px">
                            @endif
                        </td>
                        <td>
                            <form action="{{route('admin-destroy-banner',$banner->id)}}" method="POST">
                                @method("DELETE")
                                @csrf
                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>
                    </tr>
                  </tbody>
                @endforeach
          </table>
        </div>
        {{ $banners->links() }}
    </div>

</div>
@endsection
