<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','Site\Home\indexController@index');
Route::get('/verify-code/{email}','Site\Home\indexController@verifyCode')->name('verify-code');
Route::post('/verify-code/{email}','Site\Home\indexController@verifyAccount')->name('verifyAccount');
Route::get('/categories/{category_name}/{category_id}/{sub_cat_id?}','Site\Home\ProductsController@index')->name('products');
Route::post('/Cart/{product_id}','Site\Home\ProductsController@addToCart')->name('addToCart');
Route::get('/ProductDetails/{product}','Site\Home\ProductsController@view')->name('show-product');
Route::get('/sub_category/{sub_cat_id}','Site\Home\ProductsController@filter')->name('filter');
Route::post('/login', 'Site\Home\indexController@login')->name('login');
Route::get('/cart','Site\Home\CartController@index')->name('viewCart');
Route::get('/remove_from_cart/{cart_id}','Site\Home\CartController@destroy')->name('remove_from_cart');
Route::get('/add_event','Site\Home\ProductsController@add_event')->name('add_event');
Route::post('/getCategories', 'Site\Home\ProductsController@getCategories');
Route::post('/product/save', 'Site\Home\ProductsController@saveProduct')->name('product.save');
Route::get('/myproducts','Site\Home\ProfileController@index')->name('myproducts');
Route::get('/product/{id}/edit','Site\Home\ProductsController@saveProduct')->name('edit-product');
Route::Delete('/product/{id}/destroy','Site\Home\ProductsController@destroy')->name('destroy-product');
Route::get('/Product/{id}/edit','Site\Home\ProductsController@edit')->name('product.edit');
Route::patch('/Product/{id}/update','Site\Home\ProductsController@update')->name('product.update');
Route::get('/Productrate/{rate}/{product_id}','Site\Home\ProductsController@rate')->name('rate');






// Route::post('/subcat', function () {

//     $cat_id = $request->cat_id;
//     // dd($cat_id);
//     $subcategories = SubCategory::where('category_id',$cat_id)->get();

//     return response()->json([
//         $subcategories
//     ]);

// })->name('subcat');


// route::get('/products/{product}','Site\Home\ProductsController@view')->name('view_product');
// route::get('/viewproduct/{id}','ProductDetailsController@index')->name('viewproduct');
// Route::post('/sort','Site\Home\ProductsController@sort')->name('sort-category');
Auth::routes();

Route::prefix('admin')->group(function() {
    Route::get('/','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/home', 'Admin\HomeController@index')->name('admin.dashboard');
    Route::get('logout/', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/users','Admin\UsersController@index')->name('view-users');
    Route::delete('/users/{id}','Admin\UsersController@destroy')->name('admin-destroy-users');
    Route::get('/users/{id}/contact','Admin\UsersController@Contact')->name('admin-contact-users');
    Route::post('/users/{user}','Admin\UsersController@store')->name('admin-store-users');
    Route::get('/admins/create','Admin\AdminController@create')->name('create-admin');
    Route::post('/admins/store','Admin\AdminController@store')->name('store-admin');
    Route::get('/admins','Admin\AdminController@index')->name('view-admins');
    Route::delete('/admins/{id}','Admin\AdminController@destroy')->name('admin-destroy-admins');
    ////////////////////////////////////////////////////////
    Route::get('/categories','Admin\CategoryController@index')->name('view-categories');
    Route::get('/categories/create','Admin\CategoryController@create')->name('create-category');
    Route::post('/categories','Admin\CategoryController@store')->name('store-category');
    Route::get('/categories/{category}','Admin\CategoryController@show')->name('admin-show-category');
    Route::get('/categories/{category_name}/Sub-category/{id}','Admin\SubCategoryController@show')->name('admin-show-subCategory');
    Route::get('/categories/{category}/edit','Admin\CategoryController@edit')->name('admin-edit-category');
    Route::patch('/categories/{category}','Admin\CategoryController@update')->name('admin-update-category');
    Route::delete('/categories/{id}','Admin\CategoryController@destroy')->name('admin-destroy-category');
    Route::delete('/categories/{id}/delete','Admin\SubCategoryController@destroy')->name('admin-destroy-SubCategory');
    Route::get('/categories/{category}/create-sub-cat','Admin\SubCategoryController@create')->name('admin-create-sub-category');
    Route::post('/categories/{category}','Admin\SubCategoryController@store')->name('admin-store-sub-category');
    Route::get('/categories/{category_id}/SubCategory-{Subcategory}/edit','Admin\SubCategoryController@edit')->name('admin-edit-sub_category');
    Route::patch('/subcategories/{Subcategory}','Admin\SubCategoryController@update')->name('admin-update-subcategory');
    /////////////////////////////////////////////////////////
    Route::get('/countries/create','Admin\CountryController@create')->name('admin-create-country');
    Route::post('/countries','Admin\CountryController@store')->name('admin-store-country');
    Route::get('/countries','Admin\CountryController@index')->name('view-countries');
    Route::get('/countries/{country}','Admin\CountryController@show')->name('admin-show-country');
    Route::get('/countries/{country}/edit','Admin\CountryController@edit')->name('admin-edit-country');
    Route::patch('/countries/{country}','Admin\CountryController@update')->name('admin-update-country');
    Route::delete('/countries/{id}/delete','Admin\CountryController@destroy')->name('admin-destroy-country');
    Route::get('/countries/{country}/create-city','Admin\CityController@create')->name('admin-create-city');
    Route::post('/countries/{country}','Admin\CityController@store')->name('admin-store-city');
    Route::delete('/countries/{id}','Admin\CityController@destroy')->name('admin-destroy-city');

    ////////////////////////////////////////////////////////
    Route::get('/products','Admin\ProductsController@index')->name('admin-view-products');
    Route::get('/products/{product}','Admin\ProductsController@show')->name('admin-view-product_details');
    Route::post('/products/{product}','Admin\ProductsController@accept')->name('admin-accept-products');
    Route::post('/products/{id}/hold','Admin\ProductsController@hold')->name('admin-hold-products');
    Route::delete('/products/{product}','Admin\ProductsController@destroy')->name('admin-destroy-product');
    ///////////////////////////////////////////////////////////
    Route::get('/banners','Admin\BannersController@index')->name('admin-view-banners');
    Route::get('/banners/create','Admin\BannersController@create')->name('admin-add-banner');
    Route::post('/banners/store','Admin\BannersController@store')->name('admin-store-banner');
    Route::delete('/banners/{id}','Admin\BannersController@destroy')->name('admin-destroy-banner');

}) ;
