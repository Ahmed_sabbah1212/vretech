<?php

namespace App;
use App\SubCategory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['category_name', 'category_name_ar','category_image'];

    public function getSiteNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->category_name;
        else
            return $this->category_name_ar;
    }


    public function subcategory(){
        return $this->hasMany(SubCategory::class);
    }
    public function products(){
        return $this->hasMany('App\Products','cat_id');
    }
}
