@extends('layouts.layout')
@section('content')
<div class="row-fluid sortable">


    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2 class="text-center"><i class="halflings-icon user"></i><span class="break"></span>All Categories</h2>
        </div>
        <div class="box-content">

            <form class="" method="get" action="{{url("/admin/categories")}}" style="max-width:400px">
                <div style="display: flex">
                        <input type="text" class="form-control" value="{{request()->name}}" name="name" placeholder="{{trans('Search..')}}">
                        <button type="submit" class="searchbtn"><i class="fa fa-search"></i></button>
                </div>
                <style>
                    form input[type=text] {
                        padding: 10px;
                        font-size: 17px;
                        border: 1px solid grey;
                        float: left;
                        width: 80%;
                        background: #f1f1f1;
                      }
                    form .searchbtn {
                        float: left;
                        width: 20%;
                        padding: 10px;
                        background: #1e8090;
                        color: white;
                        font-size: 17px;
                        border: 1px solid grey;
                        border-left: none;
                        cursor: pointer;
                      }
                      form button:hover {
                        background: #155d69;
                      }
                </style>
            </form>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                  <tr>
                    <th> Category ID</th>
                    <th> Name</th>
                    <th> Image</th>
                  </tr>
              </thead>
              <?php $i=1;?>
                @foreach($categories as $category)
                  <tbody>
                    <tr>
                        <td ><?php echo $i; $i=$i+1;?></td>
                        <td ><a href="{{route('admin-show-category',$category->id)}}">{{$category->category_name}}</a></td>
                        <td >
                            @if(empty($category->category_image))
                                <img src="{{url('images/categories/cats.png')}}" width="100px" height="100px">
                            @else
                                <img src="{{ url('images/categories/'.$category->category_image) }}"width="100px" height="100px">
                            @endif
                        </td>
                        <td>
                            <form action="{{route('admin-destroy-category',$category->id)}}" method="POST">
                                @method("DELETE")
                                @csrf
                                <button type="submit" onclick="return confirm('Plese Note : you will delete all SubCatregories and products that are related with this category. Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>
                    </tr>
                  </tbody>
                @endforeach
          </table>
          {{ $categories->links() }}
        </div>
    </div>
</div>
@endsection
