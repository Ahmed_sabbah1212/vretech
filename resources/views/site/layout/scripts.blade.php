<!-- Scripts -->
{{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}

<script src="{{asset("/assets/site")}}/js/jquery.min.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/tether.min.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/bootstrap.min.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/slick.min.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/countdown.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/magnific-popup.min.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/jquery-ui-min.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/jquery.spinner.min.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/carousel-touch.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/switcher.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/main.js" type="5edc804143f7f98eb782d80b-text/javascript"></script>
<script src="{{asset("/assets/site")}}/js/rocket-loader.min.js" data-cf-settings="5edc804143f7f98eb782d80b-|49" defer=""></script>

{{--
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
--}}
{{--
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
--}}



{{--<div id="color_switcher_preview">--}}
{{--    <h2>أختر لونك المفضل  <a href="#"><i class="fas fa-palette"></i></a></h2>--}}
{{--    <div>--}}
{{--        <ul class="colors" id="color1">--}}
{{--            <li><a href="#" class="stylesheet"></a></li>--}}
{{--            <li><a href="#" class="stylesheet_1"></a></li>--}}
{{--            <li><a href="#" class="stylesheet_2"></a></li>--}}
{{--            <li><a href="#" class="stylesheet_3"></a></li>--}}
{{--            <li><a href="#" class="stylesheet_4"></a></li>--}}
{{--            <li><a href="#" class="stylesheet_5"></a></li>--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</div>--}}

@yield('extra_js')

