<?php

namespace App\Http\Controllers\Admin;
use App\Banners;
use App\Http\Controllers\Controller;
use Str;
use Illuminate\Http\Request;

class BannersController extends Controller
{

    public function __construct(){
        $this->middleware('auth:admin');
        // $this->middleware('guest:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banners::paginate(10);
        return view('Admins.Banners.index',compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Banners $banner)
    {
        return view('Admins.Banners.create',compact('banner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $banner = Banners::create($this->DataValidation());
        $this->storeImage($banner);
        return redirect()->route('admin-view-banners');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function show(Banners $banners)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function edit(Banners $banners)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banners $banners)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banners  $banners
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banners $banners , $id)
    {
        Banners::destroy($id);
        return back();
    }

    public function DataValidation(){
        return tap(
            request()->validate([
            'title'=>'required|min:3',
            'link'=>'required'
            ]),function(){
                if(request()->hasFile('image')){

                    request()->validate([
                        'image'=>'file|image|mimes:jpeg,png,jpg|max:10240'
                    ]);

                }

        });

    }
    public function storeImage($banner){
        //dd($banner);
        if(request()->has('image')){
            $image = request('image');
            $image_name = Str::random(20);
			$exe = strtolower($image->getClientOriginalExtension());
			$image_full_name = $image_name.'.'.$exe;
			$upload_path = 'images/banners';
			$image_url = $image_full_name;
			$image->move($upload_path,$image_full_name);
            $banner->update([
                'image'=> $image_url
            ]
            );
        }
    }
}
