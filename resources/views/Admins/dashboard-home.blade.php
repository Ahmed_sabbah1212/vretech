@extends('layouts.layout')
@section('content')
<?php $i=1;?>
<div class="row">
    <div class="container">
    <div class="bd-content-title">
        <h2>Categories</h2>
    </div>
        @foreach ($categories as $category )
        <div class="card col-md-2" style=" display:inline-block;">

            @if(empty($category->category_image))
                <img src="{{url('images/categories/cats.png')}}" class="card-img-top" width="80px" height="100px">
            @else
                <img src="{{ url('images/categories/'.$category->category_image) }}" class="card-img-top" width="80px" height="100px">
            @endif
            <div class="card-body">
            <h5 class="card-title">{{ $category->category_name }}</h5>

            <a href="{{route('admin-show-category',$category->id)}}" class="btn btn-primary"> View {{$category->category_name}}</a>
            </div>
        </div>
        @endforeach
        <a href="{{url('/admin/categories')}}" class="btn">view more -></a>
    </div>
</div>


<br>

<div class="row">
    <div class="container">
    <div class="bd-content-title">
        <h2>Banners</h2>
    </div>
        @foreach ($banners as $banners )
        <div class="card col-md-5" style=" display:inline-block;">

            @if(empty($banners->image))
                <img src="{{url('images/banners/banner.png')}}" class="card-img-top" width="80px" height="100px">
            @else
                <img src="{{ url('images/banners/'.$banners->image) }}" class="card-img-top" width="80px" height="100px">
            @endif
            <div class="card-body">
            <h5 class="card-title">{{ $banners->title }}</h5>
            <h5 class="card-title"><a href="{{ $banners->link }}">{{ $banners->link }}</a></h5>
            </div>
        </div>
        @endforeach
        <a href="{{url('/admin/banners')}}" class="btn">view more -></a>
    </div>
</div>


<br>


<div class="row">
    <div class="container">
    <div class="bd-content-title">
        <h2>Users</h2>
    </div>
        <div name="users">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>User ID</th>
                    <th> Name</th>
                    <th> Email</th>
                    <th> Phone</th>
                    <th> Address</th>
                    </tr>
                </thead>
                @foreach($users as $user)
                    <tbody>
                    <tr>
                        <td ><?php echo $i; $i=$i+1;?></td>
                        <td >{{$user->id}}</td>
                        <td >{{$user->name}}</td>
                        <td >{{$user->email}}</td>
                        <td >{{$user->mobile}}</td>
                        <td >{{ $user->city->city_name}}, {{ $user->country->country_name}}</td>
                        <td >{{$user->status}}</td>
                    </tr>
                    </tbody>
                    @endforeach

            </table>
            <a href="{{url('/admin/users')}}" class="btn">view more -></a>
        </div>
    </div>
</div>

<br>


<div class="row">
    <div class="container">
    <div class="bd-content-title">
        <h2>Countries</h2>
    </div>
        <div name="Countries">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                    <th> #</th>
                    <th> Country ID</th>
                    <th> Country Code</th>
                    <th> Country Name</th>
                    </tr>
                </thead>
                <?php $i=1;?>
                @foreach($countries as $country)
                    <tbody>
                    <tr>
                        <td ><?php echo $i; $i=$i+1;?></td>
                        <td >{{$country->id}}</td>
                        <td >{{$country->country_code}}</td>
                        <td >{{$country->country_name}}</td>

                    </tr>
                    </tbody>
                    @endforeach

            </table>
            <a href="{{url('/admin/countries')}}" class="btn">view more -></a>
        </div>
    </div>
</div>

<br>


<div class="row">
    <div class="container">
    <div class="bd-content-title">
        <h2>Products</h2>
    </div>
        <div name="Countries">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>Product Name</th>
                      <th>Available QYY</th>
                      <th>Available On</th>
                      <th>Status</th>
                      <th>category Name</th>
                      <th>SubCategory Name</th>
                      <th>Location</th>
                      <th>Price</th>
                      <th>Image</th>
                    </tr>
                </thead>
                <?php $i=1;?>
                  @foreach($products as $product)
                    <tbody>
                      <tr>
                          <td ><?php echo $i; $i=$i+1;?></td>
                          <td>{{ $product->name }}</td>
                          <td>{{ $product->available_quantity }}</td>
                          <td>{{ $product->available_date }}</td>
                          <td >{{$product->status}}</td>
                          <td>{{$product->categories->category_name}}</td>
                          <td >{{$product->subcategory->subcategory_name}}</td>
                          <td >{{$product->location}}</td>
                          <td >{{$product->price}}</td>
                          <td >
                              @if(!empty($product->image))
                              <img src="{{url('images/products/'.$product->image)}}"class="make_bigger" width="100px" height="100px">
                          @else
                              <img src="{{ url('images/products/defualt.png') }}"width="100px" height="100px">
                          @endif
                          </td>

                      </tr>
                    </tbody>
                  @endforeach
            </table>
            <a href="{{url('/admin/products')}}" class="btn">view more -></a>
        </div>
    </div>
</div>


@endsection
