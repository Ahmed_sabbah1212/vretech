@extends('layouts.layout')
@section('content')
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2 class="text-center"><i class="halflings-icon user"></i><span class="break"></span>All Admins</h2>
        </div>


        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            {{$errors->first()}}
        </div>
        @endif
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                  <tr>
                    <th> #</th>
                    <th> Name</th>
                    <th> Email</th>
                    <th> Delete</th>
                  </tr>
              </thead>
                @foreach($admins as $admin)
                  <tbody>
                    <tr>
                        <td >{{$admin->id}}</td>
                        <td >{{$admin->name}}</td>
                        <td >{{$admin->email}}</td>
                        <td>
                            <form action="{{ route('admin-destroy-admins',$admin->id) }}" method="POST">
                                @method("DELETE")
                                @csrf
                                <button type="submit"onclick="return confirm('Are you sure?')"class="btn btn-outline-danger btn-sm">Delete</button>

                            </form>

                        </td>
                    </tr>
                  </tbody>
                @endforeach
          </table>
            <div >
                {{ $admins->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
