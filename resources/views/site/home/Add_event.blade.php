@extends('site.layout.container')
@section('content')
@if (Session::has('error'))
   <div class="alert alert-danger text-center">{{ Session::get('error') }}</div>
   @elseif(Session::has('success'))
    <div class="alert alert-success text-center">{{ Session::get('success') }}</div>
@endif

<div class="container">
    <form action="{{ route('product.save') }}"method="POST" enctype="multipart/form-data" class="col-lg-12">
        @csrf
        <div class="row" >
            @include('site.home.form')
        </div>
        <input type="submit" class="btn btn-success" value="{{trans('language.submit')}}">
    </form>
</div>
@endsection

