<?php
namespace App\Http\Controllers\Site\Home;
use App\Http\Controllers\Controller;
use App\Products;
use App\SubCategory;
use App\Category;
use App\City;
use App\Cart;
use App\User;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use Session;

class ProfileController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
        // $this->middleware('guest:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

            $products = Products::where('owner_id',Auth::user()->id)->paginate(15);



        foreach($products as $product){
            $product_img_array = explode('|', $product->image);
            $product->image =$product_img_array[0];
        }

            return view('site.home.UserProducts',compact('products'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public  function show(User $user)
    {


    }


    public static function isLogin(){
        if(Auth::check())
        {
            return true;
        }
        else{
            return false;
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request )
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }
}
