<div class="tr-breadcrumb text-center bg-image" style="background-image: url(../../assets/site/images/bg/breadcrumb-bg.jpg);">
    <div class="container">
    <div class="page-title">
    <h1>{{trans('language.ourproductlist')}}</h1>
    <h2>{{trans('language.ourproducts')}}</h2>
    </div>
    </div>
    </div>
