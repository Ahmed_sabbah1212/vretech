<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $fillable = [
        'name',
        'image',
        'text'
    ];
    public function getImageAttribute($value){
        return 'http://www.vretech.com/images/about_us/'.$value;
    }
}
