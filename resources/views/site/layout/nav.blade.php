<div class="tr-topbar">
    <div class="container">
        <div class="topbar-content">
            <div class="float-left">
                <div class="price_slider_amount mt-0">
                    <button class="btn btn-primary">{{trans('language.buildyourevent')}}</button>
                </div>
            </div>
            <div class="float-right">
                <div class="user-option">
                    <span class="icon icon-avatar"></span>
                    @guest
                        <a href="{{ route('login') }}">{{trans('language.login')}}</a> /
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">{{trans('language.register')}}</a>
                        @endif
                    @else
                        <ul>
                            <li class="nav-item dropdown">
                                <div>
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                </div>
                            </li>
                            <li
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                {{trans('language.logout')}}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            </li>

                        </ul>




                    @endguest
                </div>
            </div>
        </div>
    </div>


</div>
<div class="tr-menu">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="">
                <a class="navbar-brand" href="{{url('/')}}"><img class="img-fluid" src="{{asset("/assets/site")}}/images/logo_head.png"  alt="Logo"></a>
            </div>
            <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                <ul class="navbar-nav tr-d-md-none">
                    <li class="nav-item tr-dropdown active">
                        <a class="nav-link" href="{{url('/')}}">{{trans('language.home')}}</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('add_event') }}">{{trans('language.addevent')}}</a></li>
                    @guest
                        @else
                        <li class="nav-item"><a class="nav-link" href="{{ route('myproducts') }}">{{trans('language.myproducts')}}</a></li>
                    @endguest
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="javascript:void(0)"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(app()->getLocale() == 'en')
                                En
                            @else
                                عربي
                            @endif
                        </a>
                        <div class="dropdown-menu dropdown-menu-{{app()->getLocale() == 'en' ? 'right' : 'left'}} scale-up">
                            @if(app()->getLocale() == 'en')
                                <a class="dropdown-item" href="{{ url("".url()->full()."?&lang=ar") }}">{{trans('اللغه العربيه')}}</a>

                            @else

                                <a class="dropdown-item" href="{{ url("".url()->full()."?&lang=en") }}">{{trans('English')}}</a>
                            @endif
                        </div>
                    </li>

                    <li class="nav-item"><a class="nav-link" href="landing.html">{{trans('language.contactus')}}</a></li>
                </ul>
                <ul class="navbar-nav d-lg-none">
                    <li class="nav-item tr-dropdown active">
                        <a class="nav-link" href="{{url('/')}}">{{trans('language.home')}}</a>
                    </li>

                    @if(app()->getLocale() == 'en')

                        <li class="nav-item"><a class="nav-link" href="{{ url("".url()->full()."?&lang=ar") }}">اللغه العربيه</a></li>
                    @else
                        <li class="nav-item"><a class="nav-link" href="{{ url("".url()->full()."?&lang=en") }}">ُEnglish</a></li>


                    @endif

                    <li class="nav-item"><a class="nav-link" href="contact.html">{{trans('language.contactus')}}</a></li>
                </ul>
            </div>
            <div class="find-option float-right">
                <ul class="global-list">
                    <li class="tr-search">
                        <div class="search-icon"><span class="icon icon-magnifying-glass"></span></div>
                        <div class="search-form">
                            <form action="#" id="search" method="get">
                                <input id="inputhead" name="search" type="text" placeholder="Type Your Words & Press Enter">
                                <button type="submit"><span class="icon icon-magnifying-glass"></span></button>
                            </form>
                        </div>
                    </li>
                    <li class="cart-content tr-dropdown">
                        <a href="#"><span class="icon icon-shopping-basket"></span> <span class="cart-number">3</span></a>
                        <div class="tr-dropdown-menu">
                            <ul class="global-list">
                                <li class="remove-item">
                                    <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    <div class="product">
                                        <a href="details.html">
              <span class="product-image">
         <img src="{{asset("/assets/site")}}/images/cart.png" alt="Image" class="img-fluid">
              </span>
                                            <span class="product-title"> <strong>Chair Cabbage</strong></span>
                                            <span class="price"><del>$15.00</del>$12.00</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="remove-item">
                                    <span class="remove-icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    <div class="product">
                                        <a href="details.html">
            <span class="product-image">
              <img src="{{asset("/assets/site")}}/images/cart.png" alt="Image" class="img-fluid">
           </span>
                                            <span class="product-title"> <strong>Chair Cabbage</strong></span>
                                            <span class="price"><del>$15.00</del>$12.00</span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                            <div class="total-price">
                                <span><strong>Total Price: </strong>$598:00</span>
                            </div>
                            <div class="buttons">
                                <a class="btn btn-primary cart-button" href="{{asset("/assets/site")}}/shopping-cart.html">View Cart</a>
                                <a class="btn btn-primary" href="#">Checkout</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

