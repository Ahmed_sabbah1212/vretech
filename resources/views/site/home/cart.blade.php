@extends('site.layout.container')
@section('content')
    <div class="tr-breadcrumb text-center bg-image" style="background-image: url(assets/site/images/bg/breadcrumb-bg.jpg);">
        <div class="container">
            <div class="page-title">
                <h1>Your Wish List</h1>
                <h2>Wish List </h2>
            </div>
        </div>
    </div>
    <div class="main-content popup-one section-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 push-md-4 push-lg-3 ">
                    <div class="col-12">
                        <form action="#">
                            <div class="table-content table-responsive rtl_dir">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="plantmore-product-remove">remove</th>
                                            <th class="plantmore-product-thumbnail">images</th>
                                            <th class="cart-product-name">Product</th>
                                            <th class="plantmore-product-price">Unit Price</th>
                                            <th class="plantmore-product-quantity">quantity</th>
                                            <th class="plantmore-product-status">Status</th>
                                            <th class="plantmore-product-start-date">Start Date</th>
                                            <th class="plantmore-product-end-date">End Date</th>
                                            <th class="plantmore-product-total-days">Total Days</th>
                                            <th class="plantmore-product-total-price">Total Price Per Item</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($cart_items as $cart_item)


                                        <tr>
                                            <td class="plantmore-product-remove"><a href="{{ route('remove_from_cart',$cart_item->product_id) }}"><i class="fa fa-times"></i></a></td>
                                            <td class="plantmore-product-thumbnail"><a href="{{route('show-product', $cart_item->product_id)}}"><img alt="" src="{{ url('images/products/'.$cart_item->image) }}" width="80" height="75"></a></td>
                                            <td class="plantmore-product-name"><a href="{{route('show-product', $cart_item->product_id)}}">{{ $cart_item->name }}</a></td>
                                            <td class="plantmore-product-price"><span class="amount">{{ $cart_item->price }}</span></td>
                                            <td class="plantmore-product-stock-status"><span class="in-stock">{{ $cart_item->quantity }}</span></td>
                                            <td class="plantmore-product-status">{{ $cart_item->status }}</td>
                                            <td class="plantmore-product-start-date">{{ $cart_item->start_date }}</td>
                                            <td class="plantmore-product-end-date">{{ $cart_item->end_date }}</td>
                                            <td class="plantmore-product-total-days">{{ $cart_item->days_num }}</td>
                                            <td class="plantmore-product-stock-status"><span class="in-stock">
                                                {{ $cart_item->total_pricePerItem }}
                                                </span></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <br>

                    </div>
                    <div class="container">
                        <div class="col-lg-3 plantmore-product-stock-status"><strong>Total Price</strong> : <span class="in-stock" style="font-size: 25px;">{{ $total_price }}</span></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>


        .row1 {
          display: -ms-flexbox; /* IE10 */
          display: flex;
          -ms-flex-wrap: wrap; /* IE10 */
          flex-wrap: wrap;
          margin: 0 -16px;
        }

        .col-25 {
          -ms-flex: 25%; /* IE10 */
          flex: 25%;
        }

        .col-50 {
          -ms-flex: 50%; /* IE10 */
          flex: 50%;
        }

        .col-75 {
          -ms-flex: 75%; /* IE10 */
          flex: 75%;
        }

        .col-25,
        .col-50,
        .col-75 {
          padding: 0 16px;
        }

        .container1 {
          background-color: #f2f2f2;
          padding: 5px 20px 15px 20px;
          border: 1px solid lightgrey;
          border-radius: 3px;
        }

        input[type=text] {
          width: 100%;
          margin-bottom: 20px;
          padding: 12px;
          border: 1px solid #ccc;
          border-radius: 3px;
        }

        label {
          margin-bottom: 10px;
          display: block;
        }

        .icon-container {
          margin-bottom: 20px;
          padding: 7px 0;
          font-size: 24px;
        }

        .btn {
          background-color: #4CAF50;
          color: white;
          padding: 12px;
          margin: 10px 0;
          border: none;
          width: 100%;
          border-radius: 3px;
          cursor: pointer;
          font-size: 17px;
        }

        .btn:hover {
          background-color: #45a049;
        }

        .a {
          color: #2196F3;
        }

        hr {
          border: 1px solid lightgrey;
        }

        span.price {
          float: right;
          color: grey;
        }

        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
        @media (max-width: 800px) {
          .row {
            flex-direction: column-reverse;
          }
          .col-25 {
            margin-bottom: 20px;
          }
        }
    </style>
        <div class="container col-lg-6">
            <div class="col-75">
              <div class="container1">
                <form action="#" method="POST">
                @csrf
                  <div class="row">
                    <div class="col-50">
                      <h3>Billing Address</h3>
                      <label for="fname"><i class="fa fa-user"></i> Full Name</label>
                      <input type="text" id="fname" name="firstname" placeholder="John M. Doe">
                      <label for="email"><i class="fa fa-envelope"></i> Email</label>
                      <input type="text" id="email" name="email" placeholder="john@example.com">
                      <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
                      <input type="text" id="adr" name="address" placeholder="542 W. 15th Street">
                      <label for="city"><i class="fa fa-institution"></i> City</label>
                      <input type="text" id="city" name="city" placeholder="New York">

                      <div class="row">
                        <div class="col-50">
                          <label for="state">State</label>
                          <input type="text" id="state" name="state" placeholder="NY">
                        </div>
                        <div class="col-50">
                          <label for="zip">Zip</label>
                          <input type="text" id="zip" name="zip" placeholder="10001">
                        </div>
                      </div>
                    </div>

                    <div class="col-50">
                      <h3>Payment</h3>
                      <label for="fname">Accepted Cards</label>
                      <div class="icon-container">
                        <i class="fa fa-cc-visa" style="color:navy;"></i>
                        <i class="fa fa-cc-amex" style="color:blue;"></i>
                        <i class="fa fa-cc-mastercard" style="color:red;"></i>
                        <i class="fa fa-cc-discover" style="color:orange;"></i>
                      </div>
                      <label for="cname">Name on Card</label>
                      <input type="text" id="cname" name="cardname" placeholder="John More Doe">
                      <label for="ccnum">Credit card number</label>
                      <input type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">
                      <label for="expmonth">Exp Month</label>
                      <input type="text" id="expmonth" name="expmonth" placeholder="September">
                      <div class="row">
                        <div class="col-50">
                          <label for="expyear">Exp Year</label>
                          <input type="text" id="expyear" name="expyear" placeholder="2018">
                        </div>
                        <div class="col-50">
                          <label for="cvv">CVV</label>
                          <input type="text" id="cvv" name="cvv" placeholder="352">
                        </div>
                      </div>
                    </div>

                  </div>
                  <label>
                    <input type="checkbox" checked="checked" name="sameadr"> Shipping address same as billing
                  </label>
                  <input type="submit" value="Continue to checkout" class="btn">
                </form>
              </div>
            </div>

          </div>

    @endsection
