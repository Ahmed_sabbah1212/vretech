<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Auth;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth:admin');

    }
    public function index(Request $request)
    {


        $users = User::orderBy("id", "ASC");
        $users = $this->filter($request , $users);


        return view('Admins.users.index',compact('users'));
    }
    public function filter($request, $users)
    {


        if ($request->user) {
            $users = User::where("name", 'LIKE', '%' . $request->user . '%')
                            ->orwhere("email", 'LIKE', '%' . $request->user . '%')
                            ->orwhere("mobile", 'LIKE', '%' . $request->user . '%')
                            ->paginate(10);
        }
        else{
            $users = User::orderBy("id", "ASC")->paginate(10);
        }

        return $users;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(request('email'));
        $data = [
            'email'=>request('email'),
            'code'=>'',
            'messege'=>request('messege')

        ];
        if(SendEmailController::mail($data) == true){
            return redirect('admin/users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if(auth::user()->id == $user->id){
        //     return redirect::back()->withErrors(['you are logged in you cannot remove this account']);
        // }
        // else{
        //     $user->delete();
        //     return redirect::back();
        // }
        User::destroy($id);
        return back();
    }
    public function contact($id){
        $user = User::findOrFail($id);
        // dd($user);
        return view ('Admins.users.contact',compact('user'));
    }
}
