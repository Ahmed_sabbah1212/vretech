<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Admin;
use Validator;
use Auth;
class AdminController extends Controller
{

    public function __construct(){
        $this->middleware('auth:admin');
        // $this->middleware('guest:admin');
    }

    public function index(Request $request){
        $admins = Admin::paginate(10);
        return view ('Admins.users.admins',compact('admins'));
     }
    public function create(Request $request){
        return view ('Admins.users.create');
    }

    public function store(Request $request){

        // $admin = Admin::create($this->DataValidation());
        // return redirect()->route('admin-view-banners');


        request()->validate([
            'name'=>'required|min:3',
            'email'=>'required|email',
            'password'=>'required|min:6'
            ]);
        $admin = new Admin;
        $admin->name = request('name');
        $admin->email = request('email');
        $admin->password =  Hash::make(request('password'));
        $admin->save();
        return redirect('/admin/users');
    }
    public static function DataValidation(){
        return tap(
            request()->validate([

            ]));
    }

    public function destroy($id)
    {

        if(auth::user()->id == $id){
            return back()->withErrors(['you are logged in you cannot remove this account , please Log out first']);
        }
        else{

            Admin::destroy($id);
            return back();
        }

    }
}
