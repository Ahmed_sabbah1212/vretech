

        <div class="form-group col-lg-12">
            <div class="form-group col-lg-12"style="display: inline-block; padding-right:30px;">
                <label for="product_name">{{trans('language.productname')}}</label>
                <input type="text" class="form-control" id="product_name" name ="product_name" aria-describedby="product_name" placeholder="{{trans('language.productname')}}" required>
                <div>{{ $errors->first('product_name') }}</div>
            </div>

            <div class="form-group col-lg-12"style="display: inline-block; padding-right:30px;">
                <label for="price">{{trans('language.price')}}</label>
                <input type="text" class="form-control" id="price" name ="price" aria-describedby="price" placeholder=" {{trans('language.price')}} $" required>
                <div>{{ $errors->first('price') }}</div>
            </div>

            <div class="form-group col-lg-12" style="display: inline-block; padding-right:30px;" required>
                <label for="category_name">{{trans('language.category')}}</label>
                <select  class="form-control main_category_id_list main_category_id"  id="main_category_id" name="main_category_id" required="required">
                    <option value="0">{{trans('language.category')}}</option>
                    @foreach(\App\Category::get() as $category)
                        <option  name="main_category_id"  value="{{$category->id}}">{{$category->site_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group category_List col-lg-12" style="display: inline-block; padding-right:30px;">
                <label for="category_name">{{trans('language.subcategory')}}</label>
                <select  id="category_id"  name="category_id"
                         class=" form-control category_id_list category_id" required>
                    <option name="category_id" class="basicOption" value="0">{{trans('language.subcategory')}}</option>
                </select>
            </div>


            <div class="form-group col-lg-12" style="display: inline-block; padding-right:30px;">
                <label for="category_name">{{trans('language.status')}}</label>
                <select  id="status" name="status"
                         class=" form-control" required>
                    <option name="status"  value="0" disabled>{{trans('language.status')}}</option>
                    <option name="status"  value="new" >New</option>
                    <option name="status"  value="old" >Old</option>
                </select>
            </div>
            <div class="form-group col-lg-12"style="display: inline-block; padding-right:30px;" >
                <label for="description">{{trans('language.description')}}</label>
                <textarea type="text" class="form-control" id="description" name ="description" aria-describedby="description" placeholder="{{trans('language.description')}}" required></textarea>
                <div>{{ $errors->first('description') }}</div>
            </div>
            <div class="form-group col-lg-6"style="display: inline-block;"required>
                <label for="avail-qty">{{trans('language.availablequantity')}}</label>
                <input type="number" min="0"pattern="[0-9]" class="form-control" id="avail-qty" name ="avail-qty" aria-describedby="avail-qty" placeholder="{{trans('language.availablequantity')}}">
                <div>{{ $errors->first('location') }}</div>
            </div>

            <div class="form-group col-lg-6"style="display: inline-block; ">
                <label>{{trans('language.date')}}</label>
            <input type="date" name = "available_date" class="form-control" required>
            </div>

            <div class="form-group col-lg-6"style="display: inline-block;">
                <label for="avail-qty">{{trans('language.newavailablequantity')}}</label>
                <input type="number" min="0"pattern="[0-9]" class="form-control" id="new-avail-qty" name ="new-avail-qty" aria-describedby="new-avail-qty" placeholder=" {{trans('language.newavailablequantity')}}">
                <div>{{ $errors->first('location') }}</div>
            </div>
            <div class="form-group col-lg-12"style="display: inline-block; padding-right:30px;" >
                <label for="location">{{trans('language.location')}}</label>
                <input type="text" class="form-control" id="location" name ="location" aria-describedby="location" placeholder=" {{trans('language.location')}}"required>
                <div>{{ $errors->first('location') }}</div>
            </div>
            <div class="form-group">
                <label for="image">{{trans('language.image')}}</label>
                <input type="file" name="image[]" value = "" id="image" multiple>
            </div>
            @csrf
        </div>



<script>

    $(document).ready(function () {
        $('.main_category_id').on('change', function () {
            var main_category_id = $('.main_category_id').val();
            $.post("{{url("/getCategories")}}",
                {
                    category_id: main_category_id,
                    _token: "{{csrf_token()}}"
                },
                function (data, status) {
                    if (data.status == true) {
                        $('.category_id_list').html('');
                        $.each(data, function () {
                            $.each(this, function (index, item) {
                                $(".category_id_list").prepend('' +
                                    '<option name="category_id"  value="' + item.id + '">' + item.subcategory_name + '</option>');
                            });
                        });
                        // first edit is here that the following option will be showen at the top of list
                        $(".category_id_list").prepend('' +
                            '<option  value="0" disabled> Select sub category</option>');
                    } else {
                        $('.category_id_list').html('');
                        $('.category_id').empty();
                        $(".category_id_list").prepend('' +
                            '<option name="category_id"  value="0">No sub categories yet </option>');
                    }
                });
        });
    });
</script>
