@extends('layouts.layout')
@section('content')
<div class="row-fluid sortable">
    <div class="box span12">
        <form action="{{ route('admin-view-products') }}"method="GET" enctype="multipart/form-data" class="col-lg-6" class="pb-5" style="display:flex">
            <label for="Category" style="float:left;padding: 6px;">Filter</label>
            <select id="category" name="category">
                <option disabled selected>Select Category</option>
                @foreach ($categories as $category )
                    @if($flag ==  $category->id )

                        <option name="category" value="{{ $category->id }}" selected>
                            {{ $category->category_name }}
                        </option>
                    @else

                        <option name="category" value="{{ $category->id }}">
                            {{ $category->category_name }}
                        </option>
                    @endif
                @endforeach

            </select>

            @if(!empty($SubCategory))
            <select id="subCategory" name="subCategory">
                <option name="subCategory" disabled selected>Select Sub Category</option>
                @foreach ($SubCategory as $Sub_Category )

                    @if($SubCatFlag == $Sub_Category->id)
                        <option name="subCategory" value="" selected>
                                {{ $Sub_Category->subcategory_name }}
                        </option>
                    @else
                        <option name="subCategory" value="{{ $Sub_Category->id }}">
                            {{ $Sub_Category->subcategory_name }}
                        </option>
                    @endif

                @endforeach
            </select>
            @endif


            <div class="input-group ">
                <button type="submit" class="btn btn-outline-primary btn-xs filter">Filter</button>
            </div>
            @method("GET")
            @csrf

        </form>
        <style>
            form select {
                padding: 6px;
                font-size: 15px;
                border: 1px solid grey;
                float: left;
                width: 50%;
                background: #f1f1f1;
              }
              form .filter {
                float: left;
                width: 60px;
                padding: 10px;
                background: #1e8090;
                color: white;
                font-size: 15px;
                border: 1px solid grey;
                border-left: none;
                cursor: pointer;
              }
              form .filter:hover {
                background: #155d69;
              }
        </style>



        <div class="box-header" data-original-title>
            <h2 class="text-center"><i class="halflings-icon user"></i><span class="break"></span>All Products</h2>
        </div>
        <div class="box-content">
            <form class="" method="get" action="{{url("/admin/products")}}" style="max-width:400px">
                <div style="display: flex">
                        <input type="text" class="form-control" value="{{request()->product}}" name="product" placeholder="{{trans('Search..')}}">
                        <button type="submit" class="searchbtn"><i class="fa fa-search"></i></button>
                </div>
                <style>
                    form input[type=text] {
                        padding: 10px;
                        font-size: 17px;
                        border: 1px solid grey;
                        float: left;
                        width: 80%;
                        background: #f1f1f1;
                      }
                    form .searchbtn {
                        float: left;
                        width: 20%;
                        padding: 10px;
                        background: #1e8090;
                        color: white;
                        font-size: 17px;
                        border: 1px solid grey;
                        border-left: none;
                        cursor: pointer;
                      }
                      form button:hover {
                        background: #155d69;
                      }
                </style>
            </form>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
              <thead>
                  <tr>
                    <th>#</th>
                    <th>Product Name</th>
                    <th>Available QYY</th>
                    <th>Available On</th>
                    <th>Status</th>
                    <th>category Name</th>
                    <th>SubCategory Name</th>
                    <th>Location</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>View Details</th>
                    <th>Approval</th>
                    <th>Delete</th>
                  </tr>
              </thead>
              <?php $i=1;?>
                @foreach($products as $product)
                  <tbody>
                    <tr>
                        <td ><?php echo $i; $i=$i+1;?></td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->available_quantity }}</td>
                        <td>{{ $product->available_date }}</td>
                        <td >{{$product->status}}</td>
                        <td>{{$product->categories->category_name}}</td>
                        <td >{{$product->subcategory->subcategory_name}}</td>
                        <td >{{$product->location}}</td>
                        <td >
                            @if(!empty($product->image))
                            <img src="{{url('images/products/'.$product->image)}}"class="make_bigger" width="100px" height="100px">
                        @else
                            <img src="{{ url('images/products/defualt.png') }}"width="100px" height="100px">
                        @endif
                        </td>
                        <td >{{$product->price}}</td>
                        <td>
                            <a href ="{{route('admin-view-product_details',$product->product_id)}}"><i class="fa fa-eye fa-4x" aria-hidden="true"></i></a>
                        </td>
                        <td>
                            @if($product->approval == 0 || $product->approval == 2)
                                <form action="{{ route('admin-accept-products',$product->product_id) }}" method="POST">
                                    @method("POST")
                                    @csrf
                                    <button type="submit"  class="btn btn-outline-success btn-sm">Approve</button>
                                </form>
                            @else
                            <form action="{{ route('admin-hold-products',$product->product_id) }}" method="POST">
                                @method("POST")
                                @csrf
                                <button type="submit" class="btn btn-outline-warning btn-sm">Refuse</button>
                            </form>
                            @endif

                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>
                        <td>
                            <form action="{{route('admin-destroy-product',$product->product_id)}}" method="POST">
                                @method("DELETE")
                                @csrf
                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-outline-danger btn-sm">Delete</button>

                            </form>
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif
                        </td>
                    </tr>
                  </tbody>
                @endforeach
          </table>
          {{ $products->links() }}

        </div>
    </div>
</div>

@endsection
