<?php

namespace App\Http\Controllers\Site\Home;
use Hash;
use App\Notification;
use App\Category;
use App\Banners;
use App\SubCategory;
use App\Products;
use Str;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\SendEmailController;
class indexController extends Controller
{
    // return home page ..
    public function index()
    {
        $categories = Category::orderBy("id", "desc")->get();
        $banners = Banners::get();
        // dd($categories);
        return view('site.home.index',compact('categories','banners'));
    }

    public function verifyAccount($email){
    $code = User::select('verify_code')->where('email',$email)->first();
    if($code->verify_code == request('verify-code')){
        User::where('email',request('email'))->update(['status'=>'verified','verify_code'=>request('verify-code')+1]);
        return redirect('/login')->with('success', 'Your account has been verified successfully, please login to continue');
    }else{
        return back();
    }
        // return view('site.auth.verify',compact('email'));
    }

    public function verifyCode($email){

        return view('site.auth.verify',compact('email'));
    }
    public function siteRegister(Request $request)
    {

        // dd('test');
        $user= $request->validate([

            'name' => 'required|string',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required',
        ]);
        if($validator->fails()){
           return back()->with('error','error message');
        }
        $user['password'] = Hash::make($user['password']);

        $Addeduser = User::create($user);

            $email = request('email');
            $verify_code = rand(1000,9000);
            User::where('email',request('email'))->update(['status'=>null,'verify_code'=>$verify_code]);
            // $status = User::status;
            $data = [
                'email'=>$email,
                'code'=>$verify_code
            ];


            if(SendEmailController::mail($data) == true){
                if($Addeduser->status == 'verified'){
                    Auth::login( $Addeduser);
                    return redirect('/');

                }
                else{
                    return redirect(route('verify-code',$data['email']));
                }

            }

        // AuthController::verifyCode();
        // alert()->success('تم التسجيل بنجاح');
        // return redirect(url('/'));
    }

    // public function login(Request $request)
    // {
    //     $request->validate([
    //         'email' => ['required', 'string', 'email', 'max:255'],
    //         'password' => ['required', 'string', 'min:6'],
    //     ]);
    //     // dd('done');
    //     if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    //         // The user is active, not suspended, and exists.

    //         $user = Auth::user();
    //         Auth::login($user);
    //         return redirect('/');
    //     } else {
    //         session()->flash('danger', 'error');
    //         return back();
    //     }
    // }


}
