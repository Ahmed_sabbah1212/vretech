<?php

namespace App\Http\Controllers\Site\Home;
use App\Http\Controllers\Controller;
use App\Products;
use App\ProductRate;
use App\SubCategory;
use App\Category;
use App\City;
use App\Cart;
use DB;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Session;
use Str;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Category $category, $category_name, $category_id, $sub_cat_id = null)
    {
        if($sub_cat_id == null){
            $products = Products::where('cat_id',$category_id)->paginate(15);
        }else{
            $products = Products::where('cat_id',$category_id)
                                ->where('sub_cat_id',$sub_cat_id)
                                ->paginate(15);
        }
        foreach($products as $product){
            $product_img_array = explode('|', $product->image);
            $product->image =$product_img_array[0];
        }
            $sub_categories = SubCategory::where('category_id',$category_id)->get();
            $categories = $this->getCategoryCounter();
            return view('site.home.Products',compact('categories','sub_categories','products'));

    }


    public function filter(Request $request,$sub_cat_id){
        // dd($sub_cat_id);
        $products = Products::where('sub_cat_id',request('id'))->get();
        return view('site.home.Products',compact('products'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        dd($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function view($product)
    {
        $productRate= array();
        $productDetails= Products::where('product_id',$product)->where('deleted_at',null)->first();
        $productRate['sum'] = ProductRate::where('product_id', $productDetails->product_id)->avg('rate');
        $productRate['sum'] = round($productRate['sum']);
        $productRate['count'] =ProductRate::where('product_id', $productDetails->product_id)->count();
        $product_img_array = explode('|', $productDetails->image);
        // dd($product_img_array);
        $productDetails->image =$product_img_array;
        $cities = City::select('id','city_name')->get();
        $categories = $this->getCategoryCounter();
        if(Auth::check()){

            $addedToCart = Cart::where('product_id',$product)->where('user_id',Auth::User()->id)->get();
            $user_id = Auth::User()->id;
            if(count($addedToCart)>0){
                $cartStatus = 1;
            }else{
                $cartStatus = 0;
            }

        }else{
            $user_id = 0;
            $addedToCart = Cart::where('product_id',$product)->get();
            $cartStatus = 0;
        }

        return view('site.home.ProductDetails',compact('productDetails','cities','categories','cartStatus','user_id','productRate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products , $id)
    {
        $product = Products::where('product_id',$id)->first();
        return view('site/home/edit_product',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id , Products $Products)
    {
        $product = Products::where('product_id',$id)->first();
        $product->name= request('name');
        $product->price = request('price');
        $product->current_stock = request('current_stock');
        $product->available_date = request('available_date');
        $product->new_available_qty = request('new_available_qty');
        $product->status = request('status');
        $product->description = request('description');
        if(request('main_category_id') == 0){
            $product->cat_id = $product->cat_id;
        }
        if(request('category_id') == 0){
            $product->sub_cat_id = $product->sub_cat_id;
        }
        if(request('image') == null)
        {
            $product->image = $product->image;
        }
        else{
            if($files=$request->file('image')){
                foreach($files as $file){
                    $image_name = Str::random(20);
                    $exe = strtolower($file->getClientOriginalExtension());
                    $image_full_name = $image_name.'.'.$exe;
                    $upload_path = 'images/products';
                    $image_url = $image_full_name;
                    $file->move($upload_path,$image_full_name);
                    $images[]=$image_full_name;
                }
            }
            $product->image = implode("|",$images);
        }
        $product->cat_id = request('main_category_id');
        $product->sub_cat_id = request('category_id');
        $product->location = request('location');
        $product->update();
        // SubCategory::where('id',$Subcategory)
        //     ->update([
        //         'subcategory_name' => request('subcategory_name'),
        //         'subcategory_name_ar' => request('subcategory_name_ar'),
        //     ]);
        return redirect('/myproducts');
    }

     public function UpdateDataValidation(){
        return tap(
            request()->validate([
                'name'=>'required',
                'price'=>'required',
                'current_stock'=>'required|numeric',
                'available_date'=>'required|after:now',
                'new_available_qty'=>'required|numeric',
                'status'=>'required',
                'description'=>'required',
                'main_category_id'=>'numeric|exists:categories,id',
                'category_id'=>'numeric|exists:sub_categories,id',
                'location'=>'required',
            ]));
    }
    // public function storeImage($category){

    //     if($files=$request->file('image')){
    //         foreach($files as $file){
    //             request()->validate([
    //                 'category_image'=>'file|image|mimes:jpeg,png,jpg|max:5120'
    //             ]);
    //             $image_name = Str::random(20);
    //             $exe = strtolower($file->getClientOriginalExtension());
    //             $image_full_name = $image_name.'.'.$exe;
    //             $upload_path = 'images/products';
    //             $image_url = $image_full_name;
    //             $file->move($upload_path,$image_full_name);
    //             $images[]=$image_full_name;
    //         }
    //     }
    //     else{
    //         return back;
    //     }
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductDetails  $productDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        $product = Products::where('product_id',$id);
        $product->delete();
        return redirect('/myproducts');
    }


    public static function getCategoryCounter(){
        $categories = Category::get();
        foreach($categories as $category){
            $productsCountPerCategory = $category->products->count();
            if($productsCountPerCategory == 0 ){
                $productsCountPerCategory = '';
            }
            else{
                $productsCountPerCategory = $productsCountPerCategory;
            }
            $category->productsCount  = $productsCountPerCategory;
        }
        return $categories;
    }

    public function addToCart(Request $request , $product_id){

        // 'product_id','quantity','days_num','start_date','end_date','address','lat','lng','user_id'


        $s = strtotime(request('start_date'));
        $e =strtotime(request('end_date'));
        $days_num = (int)(($e - $s)/86400);
        $rules = [
            'address'=>'required',
            'quantity'=>'required|numeric',
            'start_date'=>'required|after:tomorrow',
            'end_date'=>'required|after:start_date',
            'address'=>'required'
        ];
        $validator = Validator::make(request()->all(),$rules);
        if($validator->fails()){
            $validatorerror = $this->validationErrorsToString($validator->errors());
            Session::flash('message', $validatorerror);
            return back();
        }
        else{
            $Cart = new Cart;
            $Cart->product_id = $product_id;
            $Cart->quantity = request('quantity');
            $Cart->start_date = request('start_date');
            $Cart->end_date = request('end_date');
            $Cart->address = request('address');
            $Cart->days_num = $days_num;
            $Cart->user_id = Auth::User()->id;
            $Cart->save();
            Session::flash('message', "Item Added Successfully to your Cart");
            return back();
        }


    }
    public function validationErrorsToString($errArray) {
        $valArr = array();
        foreach ($errArray->toArray() as $key => $value) {
            $errStr = $key.' '.$value[0];
            array_push($valArr, $errStr);
        }
        if(!empty($valArr)){
            $errStrFinal = implode(',', $valArr);
        }
        return $errStrFinal;
    }

    public function add_event(){
        if(\Auth::check()){
            $categoris = Category::all();
            return view('Site.home.Add_event',compact('categoris'));
        }
        else{
            return redirect('login');
        }
    }

    public function getCategories(Request $request)
    {
        $categories = SubCategory::where("category_id", $request->category_id)->get();
        $data['categories'] = $categories;
        if (count($categories) > 0) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        return response()->json($data);
    }

    public function saveProduct(Request $request){
        // dd($request);
        $rules = [
            'product_name'=>'required',
            'price'=>'required',
            'main_category_id'=>'required|numeric|exists:categories,id',
            'category_id'=>'required|numeric|exists:sub_categories,id',
            'location'=>'required',
            'status'=>'required',
            'description'=>'required',
            'avail-qty'=>'required|numeric',
            'available_date'=>'required|after:now',
            'new-avail-qty'=>'required|numeric',

        ] ;
        $validator = Validator::make(request()->all(),$rules);
        $validation_errors = $validator->errors();
        if($validator->fails()){
            Session::flash('error', $validation_errors );
            return back();
        }
        else{

            if($files=$request->file('image')){
                foreach($files as $file){
                    $image_name = Str::random(20);
                    $exe = strtolower($file->getClientOriginalExtension());
                    $image_full_name = $image_name.'.'.$exe;
                    $upload_path = 'images/products';
                    $image_url = $image_full_name;
                    $file->move($upload_path,$image_full_name);
                    $images[]=$image_full_name;
                }
            }
                $product = new Products();
                $product->name = request('product_name');
                $product->price = request('price');
                $product->image = implode("|",$images);
                $product->current_stock = request('avail-qty');
                $product->available_date = request('available_date');
                $product->new_available_qty = request('new-avail-qty');
                $product->status = request('status') ;
                $product->description = request('description');
                $product->cat_id = request('main_category_id');
                $product->sub_cat_id = request('category_id');
                $product->location = request('location');
                $product->owner_id = Auth::user()->id;
                $product->save();
                Session::flash('success', 'Product Saved successfully' );
                return back();
        }
    }



    public function rate($rate , $product_id){
        // dd($product_id);
        if(Auth::check()){
            $ratedbefore = ProductRate::where('user_id',Auth::user()->id)->where('product_id',$product_id)->first();
            if($ratedbefore == null){
                $ProductRate = new ProductRate;
                $ProductRate->product_id =$product_id ;
                $ProductRate->user_id = Auth::user()->id;
                $ProductRate->rate = $rate;
                $ProductRate->save();
                return back();
            }
            else{
                $ratedbefore->rate = $rate;
                $ratedbefore->save();
                return back();
            }
        }else{
            return back()->with('error','Please Login first');
        }
    }












}
