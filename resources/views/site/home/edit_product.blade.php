@extends('site.layout.container')
@section('content')
@if (Session::has('error'))
   <div class="alert alert-danger text-center">{{ Session::get('error') }}</div>
   @elseif(Session::has('success'))
    <div class="alert alert-success text-center">{{ Session::get('success') }}</div>
@endif

<div class="container">
    <form action="{{ route('product.update',$product->product_id) }}" method="POST" enctype="multipart/form-data" class="col-lg-12">
        @method("PATCH")
        @csrf
        <div class="row" >






{{--
            'product_id','name','price','image','current_stock','available_date','new_available_qty','status','cat_id','sub_cat_id','location','owner_id','approval' --}}


        <div class="form-group col-lg-12">
            <div class="form-group col-lg-12"style="display: inline-block; padding-right:30px;">
                <label for="name">Product Name</label>
                <input type="text" class="form-control" id="name" name ="name" value="{{ old('name')?? $product->name }}" aria-describedby="name" placeholder=" product name" >
                <div>{{ $errors->first('name') }}</div>
            </div>

            <div class="form-group col-lg-12"style="display: inline-block; padding-right:30px;">
                <label for="price">price</label>
                <input type="text" class="form-control" id="price" name ="price" value ="{{ old('price')?? $product->price}}" aria-describedby="price" placeholder=" 100 $" >
                <div>{{ $errors->first('price') }}</div>
            </div>

            <div class="form-group col-lg-12" style="display: inline-block; padding-right:30px;" >
                <label for="category_name">{{trans("Category Name")}}</label>
                <select  class="form-control main_category_id_list main_category_id"  id="main_category_id" name="main_category_id" >
                    <option value="0">Select Category</option>
                    @foreach(\App\Category::get() as $category)
                        <option  name="main_category_id" value="{{$category->id}}">{{$category->category_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group category_List col-lg-12" style="display: inline-block; padding-right:30px;">
                <label for="category_name">{{trans("Sub Category Name")}}</label>
                <select  id="category_id"  name="category_id"
                         class=" form-control category_id_list category_id" required>
                    <option name="category_id" class="basicOption" value="0">Select sub category</option>
                </select>
            </div>


            <div class="form-group col-lg-12" style="display: inline-block; padding-right:30px;">
                <label for="Status">{{trans("Status")}}</label>
                <select  id="status" name="status"
                         class=" form-control" >
                    <option name="status"  value="0" disabled>Select status</option>
                    <option name="status"  value="new" >New</option>
                    <option name="status"  value="old" >Old</option>
                </select>
            </div>
            <div class="form-group col-lg-12"style="display: inline-block; padding-right:30px;" >
                <label for="description">Description</label>
                <textarea type="text" class="form-control" id="description"name ="description"  aria-describedby="description" placeholder="Describe your product" >{{ old('description')?? $product->description}}</textarea>
                <div>{{ $errors->first('description') }}</div>
            </div>
            <div class="form-group col-lg-6"style="display: inline-block;">
                <label for="current_stock">Available quantity</label>
                <input type="number" min="0"pattern="[0-9]" class="form-control" id="current_stock" name ="current_stock" value ="{{ old('current_stock')?? $product->current_stock}}"  aria-describedby="current_stock" placeholder="Enter Available quantity and if it not exist let it equal zero">
                <div>{{ $errors->first('location') }}</div>
            </div>

            <div class="form-group col-lg-6"style="display: inline-block; ">
                <label>Available Date</label>
            <input type="date" name = "available_date" class="form-control" value ="{{ old('available_date', date('Y-m-d'))?? $product->available_date}}" >
            </div>

            <div class="form-group col-lg-6"style="display: inline-block;">
                <label for="avail-qty">New Available quantity</label>
                <input type="number" min="0"pattern="[0-9]" class="form-control" id="new_available_qty" name ="new_available_qty" value ="{{ old('avail-qty')?? $product->new_available_qty}}"  aria-describedby="new_available_qty" placeholder=" Enter New Available quantity if exist, else you can ignore this field">
                <div>{{ $errors->first('location') }}</div>
            </div>
            <div class="form-group col-lg-12"style="display: inline-block; padding-right:30px;" >
                <label for="location">location</label>
                <input type="text" class="form-control" id="location" name ="location" value ="{{ old('location')?? $product->location}}" aria-describedby="location" placeholder=" location">
                <div>{{ $errors->first('location') }}</div>
            </div>
            <div class="form-group">
                <label for="image">Select Image</label>
                <input type="file" name="image[]" value = "" id="image" multiple>
            </div>
            @csrf
        </div>



<script>

    $(document).ready(function () {
        $('.main_category_id').on('change', function () {
            var main_category_id = $('.main_category_id').val();
            $.post("{{url("/getCategories")}}",
                {
                    category_id: main_category_id,
                    _token: "{{csrf_token()}}"
                },
                function (data, status) {
                    if (data.status == true) {
                        $('.category_id_list').html('');
                        $.each(data, function () {
                            $.each(this, function (index, item) {
                                $(".category_id_list").prepend('' +
                                    '<option name="category_id"  value="' + item.id + '">' + item.subcategory_name + '</option>');
                            });
                        });
                        // first edit is here that the following option will be showen at the top of list
                        $(".category_id_list").prepend('' +
                            '<option  value="0" disabled> Select sub category</option>');
                    } else {
                        $('.category_id_list').html('');
                        $('.category_id').empty();
                        $(".category_id_list").prepend('' +
                            '<option name="category_id"  value="0">No sub categories yet </option>');
                    }
                });
        });
    });
</script>

        </div>
        <input type="submit" class="btn btn-success" value="submit">
    </form>
</div>
@endsection

