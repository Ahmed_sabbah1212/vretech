@extends('.site.layout.container')
@section('content')
@if (\Session::has('error'))
    <div class=" alert alert-danger"style="text-align:center;">

            {!! \Session::get('error') !!}

    </div>
@endif
<div class="main-content bg-color">
    <div class="container">
    <div class="user-account text-center" style="background-image: url(images/bg/account-bg.html);">
    <div class="account-content">
    <div class="logo">
            <a href="index.html"><img class="img-fluid" src="{{asset("/assets/site")}}/images/logo_head.png" width="200px" height="100px" alt="Logo"></a>
    </div>
    <h1>{{trans('language.createaccount')}}</h1>
    <p>{{trans('language.sellyourinformation')}}</p>
    <form method="POST" action="{{ route('register') }}" class="tr-form">
        @csrf
    <div class="form-group">
    <input type="text" class="form-control" placeholder="{{trans('language.user_name')}}"  name="name" value="{{ old('name') }}" required="required">
    </div>
    <div class="form-group">
    <input type="email" class="form-control" placeholder="{{trans('language.email')}}" name="email" value="{{ old('email') }}"  required="required">
    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
    </div>

    <div class="form-group">
    <input type="password" class="form-control" placeholder="{{trans('language.password')}}" name="password" value="{{ old('password') }}" required="required">
    </div>
    <p>{{trans('language.privacypolicy')}}</p>



    {{--  <input type="submit" class="btn btn-primary" value="Sign Up Free">  --}}
    <button type="submit" class="btn btn-primary">
        {{trans('language.SignUpFree')}}
    </button>


    <div class="already-acount">
{{--
    <a href="signin.html">Already have an acount?</a>
--}}
    </div>
    </form>
    </div>
    </div>
    </div>
    </div>
@endsection
