<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Theme Region">
    <meta name="description" content="">
    <title>Build Event</title>

    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/animate.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/fonts.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/slick.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/structure.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/main.css">

    @if(app()->getLocale() == "ar")
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/main_ar.css">
    @endif

    <link id="preset" rel="stylesheet" href="{{asset("/assets/site")}}/css/presets/preset1.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/responsive.css">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

    <link rel="icon" href="{{asset("/assets/site")}}/images/ico/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset("/assets/site")}}/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset("/assets/site")}}/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset("/assets/site")}}/images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset("/assets/site")}}/images/ico/apple-touch-icon-57-precomposed.png">





    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>





    @yield('extra_css')
</head>
