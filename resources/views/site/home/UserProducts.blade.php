@extends('site.layout.container')
@section('content')
<div class="main-content ">
    <div class="container">
        <div class="col-md-12 col-lg-12">
    <div class="row">
@foreach ($products as $product )
            <div class="col-md-6 col-lg-3 ">
                <div class="product all ">
                        <span class="product-image">
                        <img src="{{ url('images/products/'.$product->image) }}" alt="Image" class="img-fluid">
                        </span>
                        <a href="{{route('show-product', $product->product_id)}}">
                            <span class="product-title">{{ $product->name }}</span>
                        </a>
                        <span class="price">{{ $product->price }}</span>
                        <div class="product-icon">
                        <ul class="global-list">
                        </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
</div>

@endsection
