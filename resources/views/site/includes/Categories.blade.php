<div class="widget widget_categories">
    <h3 class="widget_title">{{trans('language.categories')}}</h3>
    <ul>
{{--
        {{dd($categories)}}
--}}
        @foreach ($categories as$category )
        <li><a href="{{ route('products',['category_name' => $category->SiteName , 'category_id' => $category->id ]) }}"><img class="img" src={{ url('images/categories/'.$category->category_image) }} style = "width:40px;height:40px;border-radius: 50%; margin-right:10px;">{{ $category->category_name }}</a>{{ $category->productsCount }}</li>
        @endforeach
    </ul>

</div>
