<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->string('name');
            $table->string('price');
            $table->string('image');
            $table->integer('current_stock');//it's variable as per ussif need
            $table->timestamp('available_date')->useCurrent();
            $table->integer('new_available_qty')->default(0);//it's variable as per ussif need
            $table->string('status');// for new or used
            $table->string('description');
            $table->integer('rate');
            $table->integer('cat_id');
            $table->integer('sub_cat_id');
            $table->string('location'); // city only
            $table->integer('owner_id');
            $table->integer('approval');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
