@extends('layouts.layout')
@section('content')
<div class="container">
    <form action="{{ route('admin-update-subcategory',$Subcat->id) }}"method="POST" enctype="multipart/form-data" class="col-lg-12" class="pb-5">
        @method("PATCH")
        @csrf
    <div class="form-group pull-left col-lg-5 " style="display:inline-block;">
        <label for="subcategory_name">Sub Category Name</label>
        <input type="text" class="form-control" id="subcategory_name" value="{{ old('subcategory_name')?? $Subcat->subcategory_name}}" name ="subcategory_name" aria-describedby="subcategory_name" placeholder="Enter Sub category name in english">
        <div>{{ $errors->first('Subcategory_name') }}</div>
    </div>
    <div class="form-group  col-lg-5" class="pull-right" style="display:inline-block;">
        <label for="subcategory_name_ar" class="pull-right"> اسم التصنيف الفرعي </label>
        <input type="text" class="form-control " id="subcategory_name_ar" value="{{ old('subcategory_name_ar')?? $Subcat->subcategory_name_ar}}" name ="subcategory_name_ar" aria-describedby="subcategory_name" placeholder="برجاء ادخال اسم التصنيف الفرعي بالعربية">
        <div>{{ $errors->first('subcategory_name_ar') }}</div>
    </div>
    <div class="form-group">
        <label for="subcategory_image">Select Image</label>
        <input type="file" name="image" id="image">
        <input type="hidden" value="{{ old('subcategory_image')?? $Subcat->subcategory_image}}" name ="subcategory_image" id="subcategory_image">
    </div>
    <div class="form-group ">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </form>
</div>
@endsection
