@extends('.site.layout.container')
@section('content')
<div class="main-content bg-color">
    <div class="container">
    <div class="user-account text-center" style="background-image: url(images/bg/account-bg.html);">
    <div class="account-content">
    <div class="logo">
            <a href="index.html"><img class="img-fluid" src="{{asset("/assets/site")}}/images/logo_head.png" width="200px" height="100px" alt="Logo"></a>
    </div>
    <h1>Verify Account</h1>
    <p>{{ $email }}</p>
    <form method="POST" action="{{ route('verifyAccount',$email) }}" class="tr-form">
        @csrf
        {{--  <input type="hidden" name="email" value="{{ $email }}">  --}}
    <div class="form-group">
    <input type="text" class="form-control" placeholder="Enter Verification code" name= "verify-code" required="required">
    </div>
    <p>By continuing, you agree to our Terms of "use" and Privacy Policy.</p>
    <input type="submit" class="btn btn-primary" value="Verify Account">

    </form>
    </div>
    </div>
    </div>
    </div>
@endsection
