<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRate extends Model
{
    protected $fillable = [
        'product_id',
        'user_id',
        'rate'
    ];
    public function product(){
        return $this->belongsTo('\App\Products');
    }
}
