<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Products extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'product_id';
    protected $fillable = ['product_id','name','price','image','current_stock','available_date','new_available_qty','status','description','cat_id','sub_cat_id','location','owner_id','approval'];


    public function scopeExclude($query,$value = array())
    {
        return $query->select( array_diff( $this->fillable,(array) $value) );
    }

    public function users()
    {
    	return $this->belongsTo('\App\User', 'id');
    }
    public function subcategory()
    {
    	return $this->belongsTo('\App\SubCategory', 'sub_cat_id');
    }
    public function categories()
    {
    	return $this->belongsTo('\App\Category', 'cat_id');
    }
    public function rates(){
        return $this->hasMany('ProductRate');
    }
}
